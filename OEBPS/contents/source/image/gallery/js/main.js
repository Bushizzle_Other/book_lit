$(function(){

    function clear(c, a, b){
        while(c.indexOf(a) > 0)c = c.replace(a, b);
        return c;
    }

    var configUri = location.href.split('?config=')[1],
        folder = location.href.split('gallery')[0] + location.href.split('index.html?config=')[1].split('/')[1] + '/' + location.href.split('index.html?config=')[1].split('/')[2] + '/';

    var script = document.createElement('script');
    script.src = configUri;
    document.documentElement.appendChild(script);

    var ratio = 1.558,
        $wrap = $('.slider__wrapper'),
        $win = $('.slider__window'),
        $thumbs,
        $images,
        $tWrap = $('.slider__thumbs-container'),
        $tLeft = $('.slider__thumbs-prev'),
        $tRight = $('.slider__thumbs-next'),
        slidesLength,
        currentSlide = 0,
        thumbsPos = 0,
        thumbSpace = 0,
        images = [],
        texts = [],
        isResizing = false;

    var interval = setInterval(function(){
        if(typeof xml != "undefined"){
            clearInterval(interval);

            parseData();

            var $images = $('.slider__window-item img'),
                imgLength = $images.length,
                imgCounter = 0;

            $images.each(function(){

                $(this).load(function(){
                    imgCounter++;

                    if(imgCounter == imgLength){
                        resizeSlider();
                    }
                });

            });

            $(window).on('resize', function(){
                if(!isResizing) resizeSlider();
            });

        }
    }, 50);

    function parseData(){
        xml = clear(xml.split('<root>')[1].split('</root>')[0], 'image', 'span');

        var $vBox = $('<div/>').append(xml);

        $vBox.find('span').each(function(){
            images.push($(this).attr('src'));
            texts.push($(this).text());
        });

        images.forEach(function(item, i){
            var $thumb = $('<div/>', {class: 'slider__thumbs-item'});
            $thumb.append($('<img/>', {src: folder + item}));
            $tWrap.append($thumb);

            var $item = $('<div/>', {class: 'slider__window-item'});
            $item.append($('<img/>', {src: folder + item})).append($('<div/>', {class: 'slider__item-text', text: texts[i]}));
            $win.append($item);

            var $img = $item.find('img');

            $img.load(function(){
                if($img.width()/$img.height() > 1.86){$img.addClass('horizontal')}
                else {$img.addClass('vertical')}
                ////console.log($img);
                ////console.log($img.width()/$img.height());

            });
        });

        $thumbs = $('.slider__thumbs-item');
        $images = $('.slider__window-item');
        slidesLength = $images.length;

        $thumbs.click(function(){
            currentSlide = $(this).index();
            $thumbs.removeClass('active').eq(currentSlide).addClass('active');
            $images.removeClass('active').eq(currentSlide).addClass('active');
        });

        $tLeft.on('click', function(){move(-1)});
        $tRight.on('click', function(){move(1)});
        $images.on('click', function(){move(1)});


        $images.eq(0).addClass('active');
        $thumbs.eq(0).addClass('active');
    }

    function resizeSlider(){
        isResizing = true;
        if(window.innerWidth > window.innerHeight*ratio){
            $wrap.css({height: '90%', margin: '0 auto'}).css({'width': $wrap.height()*ratio + 'px'});
        } else {
            $wrap.css({width: '90%', margin: '5vh 5% 0'}).css({'height': $wrap.width()/ratio + 'px'});
        }

        $thumbs.css('width', $thumbs.height() + 'px');
        $thumbs.css('margin', '0 ' + $thumbs.height()/5.5 + 'px');
        thumbSpace = $thumbs.width()+($thumbs.width()/5.5)*2;

        $images.each(function(){
            var $el = $(this),
                $i = $el.find('img'),
                $t = $el.find('.slider__item-text');
            $t.css({width: $i.width() + 'px', marginLeft: -$i.width()/2 + 'px'});
        });
        setTimeout(function(){isResizing = false}, 500);
    }


    function move(dir){
        currentSlide+=dir;

        if(currentSlide < 0) currentSlide = 0;
        else if (currentSlide > slidesLength - 1) currentSlide = slidesLength - 1;

        if($thumbs.length > 4){
            thumbsPos=currentSlide;
            if(thumbsPos > slidesLength - 4){thumbsPos=slidesLength - 4;}
            $tWrap.css('left', -thumbsPos*thumbSpace + 'px');
        }

        $images.removeClass('active').eq(currentSlide).addClass('active');
        $thumbs.removeClass('active').eq(currentSlide).addClass('active');
    }

});