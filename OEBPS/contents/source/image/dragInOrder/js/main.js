$(window).load(function () { //TO DO: Improve drag perfomance (calculate collisions in sides except of cycle through all collection)

    $(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    function reorient(e) {
      window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';
	events[0].mousedown = 'mousedown';
	events[1].mousedown = 'tap';

	/*Глобальные переменные**/

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var aspectRatio = 0; //widescreen 16:9

    var defaultStageWidth = 1024;
    var defaultStageHeight = 576;

	contentMargin = {x: 5, y: 5};

	//Отступ всего контента от сцены
	var marginLeft = 10;
    var marginTop = 0;

    var marginBot = 0;

    var spacer = 10;

    var orientation = 0;
    var objectCount = 0;

    var currentObject = null;

    var boundsArray = [];

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

    var autoplay = true;

    var mixer = null;
    var titlePlayer = null;

    var root = null;
    var assetArray = [];

    var mainTitle = null;

    var resultScreen = null;

    var showBorder = true;

    var windowMargin = {
        x: 0,
        y: 0
    }

    var colorSheme = null;

    var levelManager = new LevelManager();

    var mainFont = 'mainFont';

    var fontConfig = {
        fontColor: '#000000',
        fontFamily: mainFont,
        fontSize: 24,
        fontBackgroundColor: '#000000'
    }

    var playerProportions = {
        x: 250,
        y: 45
    }

    var zoomGraphic = null;
    var playerGraphic = {};

	/*Слои**/
    var playerLayer = new Kinetic.Layer();
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
    var imageContainer = new Kinetic.Layer();
	var resultLayer = new Kinetic.Layer();

    stage.add(playerLayer);
	stage.add(backgroundLayer);
	stage.add(contentLayer);
    stage.add(imageContainer);
	stage.add(resultLayer);

	/*Группы**/
	var tableGroup = new Kinetic.Group();
	var answerGroup = new Kinetic.Group();
	var objectCollection = new Kinetic.Group();

	contentLayer.add(tableGroup);
	contentLayer.add(answerGroup);
	contentLayer.add(objectCollection);

	$(window).on('resize', fitStage);

    function fitStage(){
        scaleScreen($(window).width(),  $(window).height());
    }

    function initAspectRatio(){
        if($(window).width() / $(window).height() < 1.6){
            aspectRatio = 1;
            defaultStageWidth = 1024;
            defaultStageHeight = 768;

            stage.width(defaultStageWidth);
            stage.width(defaultStageHeight);
        }
    }

    function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        windowMargin.x = ((width - defaultStageWidth * scaler) / scaler) >> 1;
        windowMargin.y = ((height - defaultStageHeight * scaler) / scaler) >> 1;

        //stage.draw();
    }

    function initWindowSize(){      
        scaleScreen($(window).width(), $(window).height());
    }

	function isColliding(firstRectangle, secondRectangle){
		var result = false;
	 
		if(!(firstRectangle.bottom < secondRectangle.top ||
			firstRectangle.top > secondRectangle.bottom ||
			firstRectangle.left > secondRectangle.right ||
			firstRectangle.right < secondRectangle.left))

				result = true;

		return result;
	 }

     var preLoader = null;

     skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(initApplication, 300);
    });

    function initApplication(){
        preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));
        
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    }

    function xmlLoadHandler(xml){
        //initAspectRatio();
        //initWindowSize();

        //marginLeft += windowMargin.x > 0 ? windowMargin.x : 0;
        //marginTop += windowMargin.y > 0 ? windowMargin.y : 0;

        root = $(xml).children('component');

        levelManager.initRoute(root);

        var fontSize = $(root).attr('fontSize');
        var fontFamily = $(root).attr('fontFamily');
        var fontColor = $(root).attr('fontColor');
        var fontBackgroundColor = $(root).attr('fontBackgroundColor');
        var _showBorder = $(root).attr('showBorder');

        if(typeof fontSize != 'undefined' && fontSize != '') fontConfig.fontSize = fontSize;
        if(typeof fontFamily != 'undefined' && fontFamily != '') fontConfig.fontFamily = fontFamily;
        if(typeof fontColor != 'undefined' && fontColor != '') fontConfig.fontColor = fontColor;
        if(typeof fontBackgroundColor != 'undefined' && fontBackgroundColor != '') fontConfig.fontBackgroundColor = fontBackgroundColor;
        if(typeof _showBorder != 'undefined' && _showBorder != ''){
            showBorder = (_showBorder == 'true');
        }

        var objects = root.children('objects').children('object');

        preloadAssets(objects);

    	initImageContainer();
    }

    function preloadPlayerGraphic(callback){
        playerGraphic.playButton = new Image();

        playerGraphic.playButton.onload = function(){
            playerGraphic.playButton.isLoaded = true;
            if(playerGraphic.stopButton.isLoaded) callback();
        }

        playerGraphic.playButton.src = skinManager.getGraphic('player/playButton.png');

        playerGraphic.stopButton = new Image();

        playerGraphic.stopButton.onload = function(){
            playerGraphic.stopButton.isLoaded = true;
            if(playerGraphic.playButton.isLoaded) callback();
        }

        playerGraphic.stopButton.src = skinManager.getGraphic('player/stopButton.png');
        
    }

    function preloadAssets(objects){
        assetCollection = [];

        var text = '';
        var picture = '';
        var sound = '';

        var assetPartLoadCounter = 0;

        var imageObj;

        var soundArray = [];

        assetPartLoadCounter++;

        zoomGraphic = new Image();

        zoomGraphic.onload = function(){
            assetPartLoadCounter--;

            if(assetPartLoadCounter == 0) initApp();
        }

        zoomGraphic.src = skinManager.getGraphic('zoomButton.png');

        $(objects).each(function(index){
            text = $(this).text();
            picture = levelManager.getRoute($(this).attr('picture'));
            sound = levelManager.getRoute($(this).attr('sound'));

            if(typeof picture != 'undefined' && picture != ''){
                assetPartLoadCounter++;

                imageObj = new Image();

                imageObj.onload = function(){
                    assetPartLoadCounter--;

                    if(assetPartLoadCounter == 0) initApp();
                }

                imageObj.src = picture;
            } else{
                imageObj = '';
            }

            if(typeof sound != 'undefined' && sound != ''){
                soundArray.push(sound);

                sound = (soundArray.length - 1);
            } else{
                sound = null;
            }

            assetCollection.push({
                sound: sound,
                picture: imageObj,
                text: text
            });
        });

        if(soundArray.length != 0){
            assetPartLoadCounter+= 2;

            preloadPlayerGraphic(function(){
                assetPartLoadCounter--;
                if(assetPartLoadCounter == 0){
                    initApp();
                }
            })

            mixer = buildSoundMixer(soundArray, function(){
                assetPartLoadCounter--;
                if(assetPartLoadCounter == 0){
                    initApp();
                }
            });
        }

        if(assetPartLoadCounter == 0) initApp(); //start app
    }

    function initApp(){
        startGame();
        //setTimeout(startGame, 50);
    }

    function startGame(){

        resultScreen = initResultScreen();

        var controllsPanel = new Kinetic.Rect({
            x: 0,
            y: $(window).height() / stage.scaler - screenMarginY - 50,
            width: $(window).width() / stage.scaler,
            height: 50,
            fill: colorSheme.bottomPanel.color,
            stroke: colorSheme.bottomPanel.stroke
        });

        backgroundLayer.add(controllsPanel);

        createButton((($(window).width() / stage.scaler) >> 1) - 60, $(window).height() / stage.scaler - 43 - screenMarginY, 140, 37, 'Ответить', answerButtonHandler);

        marginBot = 54;

        var title = root.children('title').text();
        var question = root.children('question').text();

        mainTitle = createHead(title, question);

        marginTop = mainTitle.height() + 5;

        backgroundLayer.add(mainTitle);
        //backgroundLayer.draw();

        orient = root.attr('orientation');
        orient = (typeof orient == 'undefined') ? 'horizontal' : orient;

        orientation = (orient == 'horizontal') ? 0 : 1;

        isHorizontal = orientation == 0;

        var objects = root.children('objects').children('object');

        buildObjects(assetCollection);

        setTimeout(drawApp, 300);
    }

    function drawApp(){
        preLoader.hidePreloader();
        contentLayer.batchDraw();
        backgroundLayer.batchDraw();
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
     */
    function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: mainTitle.height() + mainTitle.fontSize() * 0.2 + padding,
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function buildObjects(objects){
    	objectCount = objects.length;

    	var maxWidth;
    	var maxHeight;
    	var dx;
    	var dy;

    	var x = objectCollection.x() + marginLeft;
    	var y = objectCollection.y() + marginTop;

    	var containerSpacer = 0;

    	var totalWidth = 0;
    	var totalHeight = 0;

    	//var avaibleSpaceX = ($(window).width() / stage.scaler - spacer * (objectCount - 1) - containerSpacer - marginLeft);
        //var avaibleSpaceY = ($(window).height() / stage.scaler - spacer * (objectCount - 1) - containerSpacer - marginTop);

        /*var avaibleSpaceX = (defaultStageWidth - spacer * (objectCount - 1) - containerSpacer - marginLeft);
    	var avaibleSpaceY = (defaultStageHeight - spacer * (objectCount - 1) - containerSpacer - marginTop - 50 - screenMarginY);*/

        var avaibleSpaceX = ($(window).width() / stage.scaler - (marginLeft << 1) - spacer * (objectCount - 1));
        var avaibleSpaceY = ($(window).height() / stage.scaler - marginTop - marginBot - spacer * (objectCount - 1));

        var tempRect = new Kinetic.Rect({
            x: marginLeft,
            y: y,
            width: ($(window).width() / stage.scaler - (marginLeft << 1)),
            height: ($(window).height() / stage.scaler - marginTop - marginBot),
            fill: 'black',
            opacity: 0.4
        });

        //contentLayer.add(tempRect);

        if(orientation == 0){
    		maxWidth = avaibleSpaceX / objectCount;
    		maxHeight = avaibleSpaceY;

    		dx = maxWidth;
    		dy = 0;
    	} else{
    		maxWidth = avaibleSpaceX;
    		maxHeight = avaibleSpaceY / objectCount;    		

    		dx = 0;
    		dy = maxHeight;
    	}

    	var position = [x, y];
    	var obj;

        var actualMaxWidth = 0;
        var actualMaxHeight = 0;

    	shuffle(objects);

    	for(var i = 0; i < objects.length; i++){
    		obj = createObject(position[0], position[1], maxWidth, maxHeight, objects[i], i);

    		boundsArray[i] = [position[0], position[1]];

            if(obj.width() > actualMaxWidth) actualMaxWidth = obj.width();
            if(obj.height() > actualMaxHeight) actualMaxHeight = obj.height();

    		if(orientation == 0){
    			position[0] += obj.width() + spacer;
    			totalWidth += obj.width() + spacer;

    			if(obj.height() > totalHeight) totalHeight = obj.height();

                /*//console.log(obj.height());
                //console.log(objects[i]);*/

    		} else{
    			position[1] += obj.height() + spacer;
    			totalHeight += obj.height() + spacer;

    			if(obj.width() > totalWidth) totalWidth = obj.width();

    		}   		

    		objectCollection.add(obj);
    	};
        if(orientation == 0){
            totalWidth -= spacer;
        } else{
            totalHeight -= spacer;
        }

        if(actualMaxWidth == 0) actualMaxWidth = maxWidth;
        if(actualMaxHeight == 0) actualMaxHeight = maxHeight;

        // tempRect.width(totalWidth);
        //tempRect.height(totalHeight);

        var hCount = isHorizontal ? objects.length : 1;
        var vCount = !isHorizontal ? objects.length : 1;

        ////console.log(hCount, vCount)

        totalWidth = actualMaxWidth * hCount + spacer * (hCount - 1);
        totalHeight = actualMaxHeight * vCount + spacer * (vCount - 1);

        var objCollection = objectCollection.children;

        for(i = 0; i < objCollection.length; i++){
            objCollection[i].initContainer(actualMaxWidth, actualMaxHeight, i, spacer);
        }

    	x = ((tempRect.width()) >> 1) - (totalWidth >> 1);
    	y = ((tempRect.height()) >> 1) - (totalHeight >> 1);

    	if( x > 0 && x < tempRect.width()) objectCollection.x(x);
    	if( y > 0 && y < tempRect.height()) objectCollection.y(y);

        //objectCollection.x(0);
        //objectCollection.y(0);

    	contentLayer.add(objectCollection);
    	//contentLayer.draw();
    }

    function createObject(x, y, maxWidth, maxHeight, object, index){

        var objSheme = colorSheme.object;

    	var objGroup = new Kinetic.Group({
	    		x: x,
	    		y: y,
	    		draggable: true,
		        dragBoundFunc: function(pos) {

		        	var x = objGroup.getAbsolutePosition().x;
		        	var y = objGroup.getAbsolutePosition().y;

		        	var start = [(boundsArray[0][0] + objectCollection.x())*stage.scaleX(), (boundsArray[0][1] + objectCollection.y())*stage.scaleY()];
		        	var end = [(boundsArray[objectCount - 1][0] + objectCollection.x())*stage.scaleX(), (boundsArray[objectCount - 1][1] + objectCollection.y())*stage.scaleY()];

		        	if(orientation == 0){
		        		if(pos.x < start[0]){
		        			pos.x = start[0];
		        		}
		        		if(pos.x > end[0]){
		        			pos.x = end[0];
		        		}

		        		x = pos.x;
		        	} else{
		        		if(pos.y < start[1]){
		        			pos.y = start[1];
		        		}
		        		if(pos.y > end[1]){
		        			pos.y = end[1];
		        		}

		        		y = pos.y;	
		        	}

		          return {x: x, y: y}
		        }
	    	});

    	objGroup.on('dragmove', onDragMove);
    	objGroup.on('dragstart', setCurrentObject);
        objGroup.on('dragend', resetPositions);

        objGroup.isContainer = true;

        objGroup.on(events[isMobile].mouseover, hoverPointer);
    	objGroup.on(events[isMobile].mouseout, resetPointer);

    	objGroup.index = index;
    	objGroup.answerIndex = object.answerIndex;

        var objContainer = new Kinetic.Group({
            x: 0,
            y: 0
        });

        objGroup.containerRect = new Kinetic.Rect({
            x:0,
            y:0,
            width: 0,
            height: 0,
            stroke: (showBorder ? objSheme.stroke : ''),
            strokeWidth: (showBorder ? objSheme.strokeWidth : 0),
            cornerRadius: objSheme.cornerRadius,
            fill: objSheme.color
        });

        objGroup.playerAlign = 'center';

        objGroup.container = objContainer;

        objGroup.strokeObj = objGroup.containerRect;

        objGroup.add(objGroup.containerRect);

        objGroup.add(objContainer);

    	var imageSource = object.picture;
    	var text = object.text;
        var sound = object.sound;

    	if(typeof imageSource == 'undefined') imageSource = '';

        var maxSize = {
            x: maxWidth,
            y: maxHeight
        };

        var playerHeight = sound == null ? 0 : getActualPlayerSize(maxWidth, maxHeight * 1/3).y

    	if(text != '' && imageSource == ''){ //label is not empty
    		var label = new Kinetic.Text({ //текстовая метка
		        x: marginLeft >> 1,
		        y: 0,
		        text: text,
		        fontSize: fontConfig.fontSize,
		        fontFamily: fontConfig.fontFamily,
		        fill: fontConfig.fontColor,
		        align: 'center'
		    });

            objGroup.playerAlign = 'bot';
            objGroup.playerHeight = playerHeight;

    		//label.width(((label.width() + marginLeft) > maxWidth) ? maxWidth : label.width() + marginLeft)

            if(orientation == 0 || label.width() > (maxWidth - marginLeft)) label.width(maxWidth - marginLeft);
    		//label.height(((label.height() + marginTop) > maxHeight) ? maxHeight : label.height() + marginTop)

	    	var borderRectangle = new Kinetic.Rect({
	    		x: 0,
	    		y: 0,
	    		width: label.width() + marginLeft,
                height: (orientation != 0 || (label.height() + label.fontSize() * 0.2) > maxHeight ? maxHeight : (label.height() + label.fontSize() * 0.2)),
                //height: maxHeight,
	    		//stroke: 'white',
                fill: fontConfig.fontBackgroundColor,
	    		cornerRadius: 5
	    	});

            var maxLabelHeihgt = maxHeight - playerHeight;
            //maxLabelHeihgt = borderRectangle.height() * 0.9;

            if(label.height() > maxLabelHeihgt){
                label.height(maxLabelHeihgt);
            }

            objGroup.label = label;
            /*if(sound != null){
                if(label.height() <= maxLabelHeihgt - playerHeight) label.y(playerHeight >> 1);
            }*/

            borderRectangle.height(label.height());

            //if(label.height() < maxLabelHeihgt) label.y((maxLabelHeihgt >> 1) - (label.height() >> 1));

            //objGroup.on(events[isMobile].mouseover, hoverPointer);
            //objGroup.on(events[isMobile].mouseout, resetPointer);

            objGroup.isPlayerBot = true;

	    	//objContainer.add(borderRectangle);
    		objContainer.add(label);

    		objGroup.width(borderRectangle.width());
    		objGroup.height(borderRectangle.height());

    		////console.log(borderRectangle.height());

    		//objGroup.strokeObj = borderRectangle;

    	} else if(text != ''){ //text + picture
            var obj = createPicture(imageSource, maxWidth, maxHeight, text, false, objContainer, objGroup.containerRect);

            //contentLayer.draw();

            objGroup.labelRect = obj.labelRect;
            objGroup.label = obj.label;
            objGroup.zoomBtn = obj.zoomBtn;

            var size = obj.size;

            objGroup.width(size);
            objGroup.height(size);

            objGroup.playerAlign = 'top';

    	} else if(text == '' && imageSource != ''){ //only picture
    		/*var imgObj = imageSource;
			var image = new Kinetic.Image({
			  x: 0,
			  y: 0,
			  image: imgObj,
			  width: imgObj.width,
			  height: imgObj.height
			});

			var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, maxWidth, maxHeight);

			image.scaleX(scaler);
			image.scaleY(scaler);
			image.scaler = scaler;

			var zoomBtn = createZoomButton( image.width() * image.scaleX() - 25, image.height() * image.scaleY() - 25, image);

	    	objGroup.label = label;

    		objContainer.add(image);
            objGroup.add(zoomBtn);

			var size = Math.min(maxWidth, maxHeight);

			objGroup.width(size);
	    	objGroup.height(size);*/

            var obj = createPicture(imageSource, maxWidth, maxHeight, '', false, objContainer, objGroup.containerRect);

            //contentLayer.draw();

            objGroup.labelRect = obj.labelRect;
            objGroup.label = obj.label;
            objGroup.zoomBtn = obj.zoomBtn;

            var size = obj.size;

            objGroup.width(size);
            objGroup.height(size);

            objGroup.playerAlign = 'top';
    	}

        if(sound != null){
            var soundSpacer = (objGroup.height() == 0 ? 0 : 5);
            var player = createMusicPlayer(
                0,
                objGroup.height() + soundSpacer,
                (orientation == 0 ? maxSize.x : (objGroup.width())),
                (orientation == 0 ? maxSize.y : ((maxSize.y / 3) - soundSpacer)),
                sound,
                mixer,
                false);

            objGroup.add(player);

            objGroup.player = player;

            if(objGroup.height() == 0) {
                objGroup.playerAlign = 'center';
                /*objGroup.width(player.width());
                objGroup.height(player.height());

                if(orientation == 0){
                    objGroup.width(maxSize.x);
                } else{
                    objGroup.height(maxSize.y);
                }*/
            } else{
                if(orientation == 0){
                    objGroup.width(maxSize.x);
                    if(text != '' && imageSource == ''){
                        player.setSize(objGroup.width(), objGroup.playerHeight);
                        objGroup.height(objGroup.height() + soundSpacer + player.height());
                    } else{
                        player.setSize(objGroup.width(), (objGroup.height() * 0.2) - soundSpacer);
                    }
                    //objGroup.height(objGroup.height() + soundSpacer + player.height());
                } else{
                    objGroup.width(Math.max(objGroup.width(), player.width()));
                    //objGroup.height(objGroup.height() + soundSpacer + player.height());
                }
            }
        }

        objGroup.containerRect.hide();

        objGroup.initContainer = function(containerWidth, containerHeight, i, spacer){
            objGroup.containerRect.width(containerWidth);
            objGroup.containerRect.height(containerHeight);

            if(objGroup.width() < containerWidth) objContainer.x((containerWidth >> 1) - (objGroup.width() >> 1));
            if(objGroup.height() < containerHeight) objContainer.y((containerHeight >> 1) - (objGroup.height() >> 1));

            if(objGroup.zoomBtn){
               /* objGroup.labelRect.y();
                objGroup.labelRect.x(0);
                objGroup.labelRect.width();*/

                /*if(objGroup.labelRect){
                    objGroup.labelRect.height(containerHeight * 1/6);*/
                    //objGroup.label.y(objGroup.label.y() + (objGroup.labelRect.height() >> 1) - (objGroup.label.height() >> 1))
                    //objGroup.labelRect.height(containerHeight >> 1);
                    //objGroup.labelRect.y(-objGroup.labelRect.getParent().getParent().y())
                //}

                objGroup.zoomBtn.reallocate(0, objGroup.containerRect.height(), objGroup.containerRect.width(), containerHeight * 1/6);

                if(player != null){
                    player.y(0);
                }
                //objGroup.labelRect.y(0);
            }

            if(objGroup.player){
                if(!objGroup.playerHeight){
                    objGroup.player.setSize(containerWidth, containerHeight * 0.2);
                } else{
                    objGroup.player.setSize(containerWidth, objGroup.player.height());
                }
                
                switch(objGroup.playerAlign){
                    case "top": objGroup.player.y(0);
                        break;
                    case "bot": objGroup.player.y(containerHeight - objGroup.player.height());
                        break;
                    case "center": objGroup.player.y((containerHeight >> 1) - (objGroup.player.height() >> 1));
                        break;
                }

                /*if(objGroup.isPlayerBot){
                    objGroup.player.y(-objGroup.container.y() + containerHeight - objGroup.player.height());
                } else{                    
                    objGroup.player.y(-objGroup.container.y());
                }*/

                if(objGroup.playerHeight){
                    var actualObjectHeight = objGroup.label.height();
                    var _containerHeight = containerHeight - objGroup.player.height();
                    if(actualObjectHeight < _containerHeight) objContainer.y((_containerHeight >> 1) - (actualObjectHeight >> 1));
                    ////console.log('___: ' + actualObjectHeight, _containerHeight)
                    if(actualObjectHeight > _containerHeight){
                        objGroup.label.height(_containerHeight);
                        objGroup.label.y(10)
                    }
                }

                /*if(objGroup.player.width() < containerWidth){
                    objGroup.player.x((containerWidth >> 1) - (objGroup.player.width() >> 1))
                }*/
            }

            objGroup.width(objGroup.containerRect.width());
            objGroup.height(objGroup.containerRect.height());

            objGroup.containerRect.show();

            //var x = objectCollection.x() + marginLeft;
    		//var y = objectCollection.y() + marginTop;

            if(isHorizontal){
            	objGroup.x(objectCollection.x() + marginLeft + containerWidth * i + spacer * i);
            } else{
				objGroup.y(objectCollection.y() + marginTop + containerHeight * i + spacer * i);
            }

            boundsArray[i] = [objGroup.x(), objGroup.y()];
        }

    	objGroup.getBoundsRect = function(){
    		return {left: objGroup.x(), top: objGroup.y(), right: objGroup.x() + objGroup.width(), bottom: objGroup.y() + objGroup.height()}
    	}
    	objGroup.getColliding = function(){
    		var objectArray = objectCollection.children;
    		var firstRectangle = currentObject.getBoundsRect();
    		var secondRectangle;

    		for(var i = 0; i < objectArray.length; i++){
    			if(objectArray[i].index == currentObject.index) continue;


    			secondRectangle = objectArray[i].getBoundsRect();
    			////console.log(objectArray[i].containerRect.height())

    			if(isColliding(firstRectangle, secondRectangle)) return objectArray[i];
    		}
    		return false;
    	}

    	objGroup.getPath = function(){
    		return {x: boundsArray[objGroup.index][0] - objGroup.x(), y: boundsArray[objGroup.index][1] - objGroup.y()}
    	}

    	objGroup.swapIndex = function(collidingObject){
    		var index = collidingObject.index;
    		var distanceX = Math.abs(boundsArray[index][0] - boundsArray[objGroup.index][0]);
    		var distanceY = Math.abs(boundsArray[index][1] - boundsArray[objGroup.index][1]);
    		var path = objGroup.getPath();

    		var currentDistanceX = Math.abs(boundsArray[index][0] - objGroup.x());
    		var currentDistanceY = Math.abs(boundsArray[index][1] - objGroup.y());

    		if((currentDistanceX < (distanceX / 2) && orientation == 0) || (currentDistanceY < (distanceY / 2) && orientation == 1)){ //swap indexes
    			collidingObject.index = currentObject.index;
    			currentObject.index = index;
    		}
    	}

    	objGroup.checkIndex = function(){
    		return objGroup.index == objGroup.answerIndex;
    	}

    	objGroup.setWrong = function(){
    		objGroup.strokeObj.strokeWidth(5);
    		objGroup.strokeObj.stroke('red');
    		contentLayer.draw();
    	}

    	return objGroup;
    }

    function onDragMove(evt){
        if(currentObject == null) return;
    	var collidingObject = currentObject.getColliding();
        if(collidingObject == null) return;
    	var currentIndex = currentObject.index;
    	var path = currentObject.getPath();



    	if(collidingObject){

    		collidingObject.x(boundsArray[collidingObject.index][0]+path.x);
    		collidingObject.y(boundsArray[collidingObject.index][1]+path.y);
    		//contentLayer.draw();

    		currentObject.swapIndex(collidingObject);
    	}
    }
    function setCurrentObject(evt){
    	currentObject = evt.targetNode.getParent();
        currentObject = getContainer(currentObject);
        ////console.log(currentObject);

        //if(!('getColliding' in currentObject)) currentObject = currentObject.getParent();
        //if(currentObject.isPlayer) currentObject = currentObject.container;
    }

    function getContainer(currentObject){
        var container = currentObject;
        while(!container.isContainer){
            container = container.getParent();
            if(container == null){
                return null;
            }
        }

        return container;
    }

    function resetPositions(evt){
    	var objectArray = objectCollection.children;

    	for(var i = 0; i < objectArray.length; i++){
    		objectArray[i].x(boundsArray[objectArray[i].index][0]);
    		objectArray[i].y(boundsArray[objectArray[i].index][1]);
    		contentLayer.draw();
    	}
    }    

    function createPicture(imageSource, maxWidth, maxHeight, text, isWithSound, container, containerRect){
        var imgObj = imageSource;

        var padding = 10;

        var image = new Kinetic.Image({
          x: padding >> 1,
          y: padding >> 1,
          image: imgObj,
          width: imgObj.width,
          height: imgObj.height,
          strokeWidth: 0
        });

        var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, maxWidth, maxHeight);

        var actualWidth = scaler * imgObj.width;
        var actualHeight = scaler * imgObj.height;

        var tipSheme = colorSheme.tip;

        if(text != ''){

            var label = new Kinetic.Text({ //текстовая метка
                x: 0,
                y: 0,
                text: text,
                fontSize: 22,
                fontFamily: mainFont,
                fill: tipSheme.fontColor,
                align: 'left',
                width: actualWidth//,
                //height: 22
            });
        }

        scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, actualWidth - padding, actualHeight - (padding >> 1));

        image.scaleX(scaler);
        image.scaleY(scaler);
        image.scaler = scaler;

        var newImageSize = {
            x: imgObj.width * scaler,
            y: imgObj.height * scaler
        };

        if(newImageSize.x < (actualWidth - padding)) image.x(((actualWidth >> 1) - (newImageSize.x >> 1)))

        //label.width(image.width()*image.scaleX() + padding - (marginLeft >> 1));
        //label.height(image.height()*image.scaleY() + padding / 10);

        //if(text != ''){

            //var radius = 4;

            var labelRect = new Kinetic.Rect({
                x: 0,
                y: 0,
                //y: containerRect.height() + containerRect.y(),
                width: actualWidth,
                //height: label.fontSize() + label.fontSize() * 0.1 - 2,
                fill: tipSheme.color,
                opacity: tipSheme.opacity
            });

            //label.y(labelRect.y());
        //}

        var zoomBtn = createZoomButton(0, 0, image);

        zoomBtn.reallocate = function(x, y, width, height){

            var zoomPadding = height * 0.2;
            var zoomSize = height - (zoomPadding << 1);

            labelRect.height(height);

            y -= labelRect.height();

            labelRect.x(x);
            labelRect.y(y);
            labelRect.width(width);

            if(text != ''){

                //zoomSize = height - (zoomPadding << 1);

                label.x(x + 10);

                ////console.log('prev ' + label.height());

                label.fontSize(height * 0.8);
                if(label.height() > label.fontSize()) label.height(label.fontSize());

                ////console.log('next ' + label.height());

                //label.y(y + height * 0.1);

                if(label.height() < height){
                    label.y(y + (height >> 1) - (label.height() >> 1));
                }

                label.width(width - zoomSize - 15);

            }

            zoomBtn.resize(zoomSize);

            //zoomBtn.x(x + width - zoomSize);
            zoomBtn.x(width - zoomSize - 5);
            zoomBtn.y(y + zoomPadding);
            //zoomBtn.y(0);
        }

        //objGroup.strokeObj = image;

        container.add(image);

        container.getParent().add(labelRect);
        
        if(text != ''){            
            container.getParent().add(label);
        }
        
        container.getParent().add(zoomBtn);

        var size = Math.min(maxWidth, maxHeight);

        return {
            'labelRect': labelRect,
            'label': label,
            'size': size,
            'zoomBtn': zoomBtn 
        }

        /*objGroup.labelRect = labelRect;
        objGroup.label = label;
        
        objContainer.add(image);
        objContainer.add(labelRect);
        objContainer.add(label);

        contentLayer.draw();

        objGroup.width(size);
        objGroup.height(size);*/
    }

    function answerButtonHandler(evt){
    	var objectArray = objectCollection.children;
    	var isCorrect = true;

    	for(var i = 0; i < objectArray.length; i++){
    		if(!objectArray[i].checkIndex()){
    			isCorrect = false;
    			objectArray[i].setWrong();
    		}
    	}

        resultScreen.invoke(isCorrect);

    	//showResultLabel($(window).width() / stage.scaler - 390, $(window).height() / stage.scaler - 60 - screenMarginY, isCorrect);

    	var button = evt.targetNode.getParent();

    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 26,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        backgroundLayer.add(buttonGroup);
        //backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mousedown, resetPointer);

        return buttonGroup;
    }

    function showResultLabel(x, y, isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	//backgroundLayer.draw();
    }

    function shuffle(o){
    	for(var k = o.length; k>0; k--, o[k].answerIndex = k); //DANGER AREA!!!
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function createZoomButton(x, y, linkedImage){
		var imageObj = zoomGraphic;

        var zoomButtonGroup = new Kinetic.Group({
            x: x,
            y: y
        });

        zoomButtonGroup.width(23);
        zoomButtonGroup.height(23);

        //imageObj.onload = function() {
		var zoomButton = new Kinetic.Image({
			x: 0,
			y: 0,
			image: imageObj,
            width: zoomButtonGroup.width(),
            height: zoomButtonGroup.height()		
		});

        zoomButtonGroup.zoomButton = zoomButton;

		zoomButtonGroup.on(events[isMobile].click, function(){
			imageContainer.zoomImage(linkedImage);
		})

		zoomButtonGroup.on(events[isMobile].mouseover, hoverPointer);
		zoomButtonGroup.on(events[isMobile].mouseout, resetPointer);

        zoomButtonGroup.add(zoomButton);

			//linkedImage.getParent().add(zoomButtonGroup);
			//contentLayer.draw();
		//}

        zoomButtonGroup.resize = function(newSize){
            //if(!zoomButtonGroup.zoomButton) return;

            this.scale({
                x: newSize / this.width(),
                y: newSize / this.height()
            });

            //zoomButtonGroup.zoomButton.width(newSize);
            //zoomButtonGroup.zoomButton.height(newSize);
        }

        return zoomButtonGroup;
	}

	function initImageContainer(){
    	var image = new Kinetic.Image({
		  x: 20,
		  y: 20
		});

		var label = new Kinetic.Text({
			x: 20,
		  	y: 20
		});

		var rect = new Kinetic.Rect({
			x: 20,
		  	y: 20
		});

		var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		var modalGroup = new Kinetic.Group();

		modalRect.opacity(0.8);

		image.on(events[isMobile].mouseover, hoverPointer);
    	image.on(events[isMobile].mouseout, resetPointer);

		image.hide();
		modalRect.hide();
		rect.hide();
		label.hide();

		modalGroup.add(modalRect);
		modalGroup.add(image);
		modalGroup.add(rect);
		modalGroup.add(label);

		imageContainer.add(modalGroup);

		imageContainer.zoomImage = function(linkedImage){
			var imageObj = linkedImage.getImage();
			var objGroup = linkedImage.getParent().getParent();

			image.setImage(imageObj);

			var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight - 40 - screenMarginY);

			image.width(imageObj.width*scaler);
			image.height(imageObj.height*scaler);

			image.x(((defaultStageWidth-40)>>1)-(image.width()>>1))

			modalGroup.on(events[isMobile].click, zoomOut);

			if(!image.isVisible()){
				image.x(defaultStageWidth >> 1);
		    	image.y(defaultStageHeight >> 1);

		    	image.scale({
		    		x: 0,
		    		y: 0
		    	})

		    	var inTween = new Kinetic.Tween({
		    		node: image,
		    		scaleX: 1,
		    		scaleY: 1,
		    		x: ($(window).width() / stage.scaler >> 1) - (image.width() >> 1),
                    y: ($(window).height() / stage.scaler >> 1) - (image.height() >> 1),
					duration: 0.4,
					easing: Kinetic.Easings.EaseInOut
		    	});

		    	inTween.onFinish = function(){
		    		//modalRect.show();

		    		if( typeof objGroup.label != 'undefined' ){
						//label.setAttrs(objGroup.label.getAttrs());
						rect.setAttrs(objGroup.labelRect.getAttrs());

                        label.fontSize(objGroup.label.fontSize());
                        label.fontFamily(objGroup.label.fontFamily());
                        label.fill(objGroup.label.fill());
                        label.text(objGroup.label.text());

                        label.width(image.width()*image.scaleX() - 10);

						label.x(image.x() + 10)
						label.y((image.y() + image.height() * image.scaleX()) - (label.height() + label.fontSize() * 0.1));

						rect.x(image.x());
						rect.y(label.y());

						rect.width(label.width() + 10);
                        rect.height(label.height() + 2);

						rect.show();
						label.show();

						imageContainer.draw();
					}
		    	};

                modalRect.show();
				
				image.show();
		    	inTween.play();
			}

			imageContainer.draw();
		}

		function zoomOut(evt){
					
			modalRect.hide();

	    	var outTween = new Kinetic.Tween({
	    		node: image,
	    		scaleX: 0,
	    		scaleY: 0,
	    		x: defaultStageWidth>>1,
	    		y: defaultStageHeight>>1,
				duration: 0.4,
				easing: Kinetic.Easings.EaseInOut
	    	});

			rect.hide();
			label.hide();

	    	outTween.onFinish = function(){
		    	image.hide();
			}

	    	outTween.play();
		}
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height,
                scaleX: 1 / skinManager.getScaleFactor(),
                scaleY: 1 / skinManager.getScaleFactor()
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                //backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = skinManager.getGraphic('preLoader.png');

        return preloaderObject;
    }

    function buildSoundMixer(assetArray, onAssetsLoaded){
        var mixer = {};

        if (!createjs.Sound.initializeDefaultPlugins()) {return;}

        var audioPath = "";
        var manifest = [];

        for(var i = 0; i < assetArray.length; i++){
            if(assetArray[i] == '' || typeof assetArray[i] == 'undefined') continue;
            manifest.push({
                id: i,
                src: assetArray[i]
            });
        }

        if(manifest.length < 1){
            onAssetsLoaded();
            return;
        }
        
        mixer.loadCounter = manifest.length;

        createjs.Sound.alternateExtensions = ["ogg"];
        createjs.Sound.addEventListener("fileload", handleLoad);
        createjs.Sound.registerManifest(manifest, audioPath);

        mixer.targetUI = null;

        mixer.assignUI = function(ui){
            if(mixer.targetUI != null) mixer.targetUI.reset();
            mixer.targetUI = ui;
        }

        mixer.play = function(id){
            mixer.instance = createjs.Sound.play(id);
            mixer.instance.addEventListener ("complete", function(instance) {
                if(mixer.targetUI != null) mixer.targetUI.reset();
            });
        }

        function handleLoad(event) {
            mixer.loadCounter--;
            if(mixer.loadCounter == 0 && onAssetsLoaded) onAssetsLoaded();
        }

        return mixer;
    }

    function getActualPlayerSize(maxWidth, maxHeight){
        var scaler = calculateAspectRatioFit(playerProportions.x, playerProportions.y, maxWidth, maxHeight);
        return {
            x: playerProportions.x * scaler,
            y: playerProportions.y * scaler
        }
    }

    function createMusicPlayer(x, y, maxWidth, maxHeight, musicID, mixer, autoplay){
        var playerSheme = colorSheme.player;

        var musicPlayer = new Kinetic.Group({
            x: x,
            y: y
        });

        if(maxWidth == 0) maxWidth = playerProportions.x;
        if(maxHeight == 0) maxHeight = playerProportions.y;

        if(isMobile) autoplay = false;

        var uiLoadCounter = 2;

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: maxWidth,
            height: maxHeight,
            fill: playerSheme.backgroundRectColor,
            cornerRadius: 0
        });

        musicPlayer.strokeObj = backgroundRect;

        //var scaler = calculateAspectRatioFit(backgroundRect.width(), backgroundRect.height(), maxWidth, maxHeight);

        /*musicPlayer.scale({
            x: scaler,
            y: scaler
        });*/

        var scaler = 1;

        musicPlayer.scaler = scaler;

        musicPlayer.width(backgroundRect.width() * scaler);
        musicPlayer.height(backgroundRect.height() * scaler);

        backgroundRect.opacity(playerSheme.opacity);

        musicPlayer.add(backgroundRect);

        musicPlayer.isAssigned = false;

        musicPlayer.drawSlider = true;

        musicPlayer.controllsPadding = 10;

        musicPlayer.controlls = new Kinetic.Group({
            x: 0,
            y: 0
        });

        musicPlayer.controlls.setSize = function(newWidth, newHeight){
            musicPlayer.controllsPadding = newHeight * 0.2;
            newWidth -= (musicPlayer.controllsPadding << 1);
            newHeight -= (musicPlayer.controllsPadding << 1);

            var scaler = calculateAspectRatioFit(
                musicPlayer.controlls.actualWidth,
                musicPlayer.controlls.actualHeight,
                newWidth,
                newHeight);

            musicPlayer.controlls.scale({
                x: scaler,
                y: scaler
            });

            musicPlayer.controlls.x(musicPlayer.controllsPadding);
            musicPlayer.controlls.y(musicPlayer.controllsPadding);


            newWidth += (musicPlayer.controllsPadding << 1);
            newHeight += (musicPlayer.controllsPadding << 1);

            if(musicPlayer.controlls.actualHeight * scaler < newHeight){
                musicPlayer.controlls.y((newHeight >> 1) - ((musicPlayer.controlls.actualHeight * scaler) >> 1));
            }

            var sliderPadding = newHeight * 0.40;

            musicPlayer.slider.x(musicPlayer.controlls.actualWidth * scaler + (musicPlayer.controllsPadding * 4.5));
            musicPlayer.slider.y(sliderPadding);

            var sliderWidth = newWidth - musicPlayer.slider.x() - (musicPlayer.controllsPadding << 1);

            if(sliderWidth > 10){
                musicPlayer.slider.show();
            } else{
                musicPlayer.slider.hide();
            }
            
            musicPlayer.slider.setSize(
                (sliderWidth),
                newHeight - (sliderPadding << 1));

            //stopButton.x(musicPlayer.controllsPadding + playButton.width() * playButton.scaler);
        }

        musicPlayer.add(musicPlayer.controlls);

        var playGraphic = playerGraphic.playButton;

        //playGraphic.onload = function(){

        var graphWidth = 20;
        var graphHeight = 26.5;

        var playButton = new Kinetic.Sprite({
            x: 0,
            y: 0,
            image: playGraphic,
            animation: autoplay ? 'pause' : 'play',
            animations: {
                pause: [
                  // x, y, width, height
                  0, 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
                ],
                play: [
                  // x, y, width, height
                  graphWidth * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
                ]
            },
            frameRate: 7,
            frameIndex: 0,
            width: graphWidth * skinManager.getScaleFactor(),
            height: graphHeight * skinManager.getScaleFactor()
        });

        playButton.scale({
            x: (1 / skinManager.getScaleFactor()),
            y: (1 / skinManager.getScaleFactor())
        });

        musicPlayer.controlls.actualWidth = playButton.width() * playButton.scaleX();
        musicPlayer.controlls.actualHeight = playButton.height() * playButton.scaleY();

        musicPlayer.setStoped = function(){
            if(playButton.animation() != 'play'){
                playButton.animation('play');
                musicPlayer.getLayer().draw();
                musicPlayer.paused = false;
            }
        }

        musicPlayer.assignToMixer = function(){
            mixer.assignUI(musicPlayer);
            musicPlayer.isAssigned = true;
            mixer.play(musicID);

            playButton.animation('pause');

            musicPlayer.paused = false;

            trackTime();
        }

        musicPlayer.play = function(newSoundID){
            musicID = newSoundID;
            musicPlayer.assignToMixer();
        }

        musicPlayer.controlls.add(playButton);
        //musicPlayer.getLayer().draw();

        playButton.on(events[isMobile].mouseover, hoverPointer);
        playButton.on(events[isMobile].mouseout, resetPointer);

        playButton.on(events[isMobile].click, function(){
            if(!musicPlayer.isAssigned){
                musicPlayer.assignToMixer();
            } else{
            
                if(playButton.animation() == 'play'){
                    musicPlayer.paused ? mixer.instance.resume() : mixer.instance.play()

                    playButton.animation('pause');

                    musicPlayer.paused = false;
                } else{
                   mixer.instance.pause();
                   playButton.animation('play');
                   musicPlayer.getLayer().draw();
                   musicPlayer.paused = true;
                }
            }
        });


        //if(autoplay) musicPlayer.assignToMixer();

        //uiLoadCounter--;
        //if(uiLoadCounter == 0) onUILoaded();
        //}

        musicPlayer.destroyPlayer = function(){
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.remove();
        }

        musicPlayer.isLoaded = false;

        musicPlayer.switchMusic = function(newSoundID){
            if(musicPlayer.isLoaded){
                musicPlayer.play(newSoundID);
            } else{
                musicPlayer.uiLoadCallback = function(){
                    musicPlayer.play(newSoundID);
                }
            }
        }

        musicPlayer.uiLoadCallback = null;

        //playGraphic.src = skinManager.getGraphic('player/playButton.png');

        var stopGraphic = playerGraphic.stopButton;

        //stopGraphic.onload = function(){

        var stopButton = new Kinetic.Image({
            x: playButton.width() * playButton.scaleX() + musicPlayer.controllsPadding,
            y: 0,
            image: stopGraphic,
            width: stopGraphic.width,
            height: stopGraphic.height
        });

        stopButton.scale({
            x: (1 / skinManager.getScaleFactor()),
            y: (1 / skinManager.getScaleFactor())
        })

        var stopButtonWidth = stopButton.width() * stopButton.scaleX();
        var stopButtonHeight = stopButton.height() * stopButton.scaleY();

        musicPlayer.controlls.actualWidth += stopButtonWidth;
        //if(stopButtonWidth > musicPlayer.controlls.actualWidth) musicPlayer.controlls.actualWidth = stopButtonWidth;
        if(stopButtonHeight > musicPlayer.controlls.actualHeight) musicPlayer.controlls.actualHeight = stopButtonHeight;

        stopButton.on(events[isMobile].mouseover, hoverPointer);
        stopButton.on(events[isMobile].mouseout, resetPointer);

        stopButton.on(events[isMobile].click, function(){
            if(!musicPlayer.isAssigned) return;
            mixer.instance.stop();
            musicPlayer.setStoped();
        });

        musicPlayer.controlls.add(stopButton);
        //musicPlayer.getLayer().draw();

        //uiLoadCounter--;
        //if(uiLoadCounter == 0) onUILoaded();
        //}

        function onUILoaded(){
            if(autoplay){
                setTimeout(function(){
                    titlePlayer.switchMusic(currentTask);
                }, 200);
            } else{
                if(musicPlayer.uiLoadCallback){
                    musicPlayer.uiLoadCallback();
                    musicPlayer.uiLoadCallback = null;
                }
            }

            //musicPlayer.getLayer().batchDraw();
        }

        stopGraphic.src = skinManager.getGraphic('player/stopButton.png');

        musicPlayer.slider = new Kinetic.Group({
            x: 55 + (musicPlayer.controllsPadding << 1),
            y: 10 + musicPlayer.controllsPadding
        })

        var sliderRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: backgroundRect.width() - 84,
            height: 7,
            fillLinearGradientStartPoint: {x: 0, y: 0},
            fillLinearGradientEndPoint: {x: backgroundRect.width() - 84, y: 7},
            fillLinearGradientColorStops: [0, playerSheme.trackColor[0], 1, playerSheme.trackColor[1]],
            scaleX: 0
        });

        var sliderBackground = new Kinetic.Rect({
            x: sliderRect.x(),
            y: sliderRect.y(),
            width: sliderRect.width(),
            height: sliderRect.height(),
            fill: playerSheme.trackBackroundColor
        });

        musicPlayer.slider.setSize = function(newWidth, newHeight){
            sliderRect.width(newWidth);
            sliderRect.height(newHeight);
            sliderBackground.width(newWidth);
            sliderBackground.height(newHeight);
        }

        musicPlayer.instance = null;
        musicPlayer.currentTrack = '';

        musicPlayer.on(events[isMobile].click, function(){
            if(!musicPlayer.isAssigned) return;
            //if(musicPlayer.paused) mixer.instance.resume();
            var pointer = stage.getPointerPosition();
            var rectPos = sliderRect.getAbsolutePosition();

            var area = {
                left: rectPos.x,
                top: rectPos.y,
                right: rectPos.x + sliderRect.width() * stage.scaler * musicPlayer.scaleX(),
                bottom: rectPos.y + sliderRect.height() * stage.scaler * musicPlayer.scaleY()
            };

            if(pointer.x > area.left && pointer.x < area.right && pointer.y > area.top && pointer.y < area.bottom){
                var scaler = (pointer.x - area.left) / (sliderRect.width() * stage.scaler * musicPlayer.scaleX());
                mixer.instance.setPosition(mixer.instance.getDuration() * scaler);
            }
        });
        
        musicPlayer.slider.add(sliderBackground);
        musicPlayer.slider.add(sliderRect);

        musicPlayer.add(musicPlayer.slider);

        musicPlayer.setSize = function(newWidth, newHeight){
            musicPlayer.controlls.setSize(newWidth, newHeight);

            ////console.log(musicPlayer.width() * musicPlayer.scaleX(), musicPlayer.height() * musicPlayer.scaleY(), newSize)

            backgroundRect.width(newWidth);
            backgroundRect.height(newHeight);

            musicPlayer.scale({
                x: 1,
                y: 1
            })

            musicPlayer.width(backgroundRect.width());
            musicPlayer.height(backgroundRect.height());

            musicPlayer.x(0);

            /*musicPlayer.scale({
                x: scaler,
                y: scaler
            });

            musicPlayer.width(playerProportions.x * scaler);
            musicPlayer.height(playerProportions.y * scaler);*/
        }

        musicPlayer.reset = function(){
            sliderRect.scaleX(0);
            musicPlayer.setStoped();
            mixer.instance.stop();
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.isAssigned = false;
        }

        setTimeout(onUILoaded, 100);

        return musicPlayer;

        function trackTime() {
            if(!musicPlayer.layerProcessed){
                musicPlayer.getLayer().drawScene.process();
                musicPlayer.layerProcessed = true;
            }

            musicPlayer.positionInterval = setInterval(function(event) {
                if(musicPlayer == null){
                    clearInterval(this);
                    return;
                }
                sliderRect.scaleX(mixer.instance.getPosition() / mixer.instance.getDuration());
                if(musicPlayer.drawSlider) musicPlayer.getLayer().drawScene();

            }, isMobile ? 100 : 30);
        }
    }

    /*function createMusicPlayerOld(x, y, maxWidth, maxHeight, musicID, mixer, autoplay){
        var musicPlayer = new Kinetic.Group({
            x: x,
            y: y
        });

        if(isMobile) autoplay = false;

        var uiLoadCounter = 2;

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 250,
            height: 40,
            fill: '#666666',
            cornerRadius: 4
        });

        var scaler = calculateAspectRatioFit(backgroundRect.width(), backgroundRect.height(), maxWidth, maxHeight);

        musicPlayer.scale({
            x: scaler,
            y: scaler
        });

        musicPlayer.strokeObj = backgroundRect;

        musicPlayer.width(maxWidth * scaler);
        musicPlayer.height(maxHeight * scaler);

        backgroundRect.opacity(0.60);

        musicPlayer.add(backgroundRect);

        musicPlayer.isAssigned = false;

        musicPlayer.drawSlider = true;

        var playGraphic = new Image();

        playGraphic.onload = function(){
            var playButton = new Kinetic.Sprite({
                x: 8,
                y: 8,
                image: playGraphic,
                animation: autoplay ? 'pause' : 'play',
                animations: {
                    play: [
                      // x, y, width, height
                      0,0,93,93
                    ],
                    pause: [
                      // x, y, width, height
                      93,0,93,93
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            playButton.scale({
                x: 0.28,
                y: 0.28
            })

            musicPlayer.setStoped = function(){
                if(playButton.animation() != 'play'){
                    playButton.animation('play');
                    musicPlayer.getLayer().draw();
                    musicPlayer.paused = false;
                }
            }

            musicPlayer.assignToMixer = function(){
                mixer.assignUI(musicPlayer);
                musicPlayer.isAssigned = true;
                mixer.play(musicID);

                playButton.animation('pause');

                musicPlayer.paused = false;

                trackTime();
            }

            musicPlayer.play = function(newSoundID){
                musicID = newSoundID;
                musicPlayer.assignToMixer();
            }

            musicPlayer.add(playButton);
            musicPlayer.getLayer().draw();

            playButton.on(events[isMobile].mouseover, hoverPointer);
            playButton.on(events[isMobile].mouseout, resetPointer);

            playButton.on(events[isMobile].click, function(){
                if(!musicPlayer.isAssigned){
                    musicPlayer.assignToMixer();
                } else{
                
                    if(playButton.animation() == 'play'){
                        musicPlayer.paused ? mixer.instance.resume() : mixer.instance.play()

                        playButton.animation('pause');

                        musicPlayer.paused = false;
                    } else{
                       mixer.instance.pause();
                       playButton.animation('play');
                       musicPlayer.getLayer().draw();
                       musicPlayer.paused = true;
                    }
                }
            });


            //if(autoplay) musicPlayer.assignToMixer();

            uiLoadCounter--;
            if(uiLoadCounter == 0) onUILoaded();
        }

        musicPlayer.destroyPlayer = function(){
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.remove();
        }

        musicPlayer.isLoaded = false;

        musicPlayer.switchMusic = function(newSoundID){
            if(musicPlayer.isLoaded){
                musicPlayer.play(newSoundID);
            } else{
                musicPlayer.uiLoadCallback = function(){
                    musicPlayer.play(newSoundID);
                }
            }
        }

        musicPlayer.uiLoadCallback = null;

        playGraphic.src = 'assets/player/playButton.png';

        var stopGraphic = new Image();

        stopGraphic.onload = function(){

            var stopButton = new Kinetic.Image({
                x: 38,
                y: 8,
                image: stopGraphic
            });

            stopButton.scale({
                x: 0.28,
                y: 0.28
            })

            stopButton.on(events[isMobile].mouseover, hoverPointer);
            stopButton.on(events[isMobile].mouseout, resetPointer);

            stopButton.on(events[isMobile].click, function(){
                if(!musicPlayer.isAssigned) return;
                mixer.instance.stop();
                musicPlayer.setStoped();
            });

            musicPlayer.add(stopButton);
            musicPlayer.getLayer().draw();

            uiLoadCounter--;
            if(uiLoadCounter == 0) onUILoaded();
        }

        function onUILoaded(){
            if(autoplay){
                setTimeout(function(){
                    titlePlayer.switchMusic(currentTask);
                }, 200);
            } else{
                if(musicPlayer.uiLoadCallback){
                    musicPlayer.uiLoadCallback();
                    musicPlayer.uiLoadCallback = null;
                }
            }
        }

        stopGraphic.src = 'assets/player/stopButton.png';

        var sliderRect = new Kinetic.Rect({
            x: 71,
            y: 18,
            width: backgroundRect.width() - 84,
            height: 7,
            fillLinearGradientStartPoint: {x: 0, y: 0},
            fillLinearGradientEndPoint: {x: backgroundRect.width() - 84, y: 7},
            fillLinearGradientColorStops: [0, '#D9F0FE', 1, '#99D7FE'],
            scaleX: 0
        });

        var sliderBackground = new Kinetic.Rect({
            x: sliderRect.x(),
            y: sliderRect.y(),
            width: sliderRect.width(),
            height: sliderRect.height(),
            fill: '#DFDFDF'
        });

        musicPlayer.instance = null;
        musicPlayer.currentTrack = '';

        musicPlayer.on(events[isMobile].click, function(){
            if(!musicPlayer.isAssigned) return;
            //if(musicPlayer.paused) mixer.instance.resume();
            var pointer = stage.getPointerPosition();
            var rectPos = sliderRect.getAbsolutePosition();

            var area = {
                left: rectPos.x,
                top: rectPos.y,
                right: rectPos.x + sliderRect.width() * stage.scaler * musicPlayer.scaleX(),
                bottom: rectPos.y + sliderRect.height() * stage.scaler * musicPlayer.scaleY()
            };

            if(pointer.x > area.left && pointer.x < area.right && pointer.y > area.top && pointer.y < area.bottom){
                var scaler = (pointer.x - area.left) / (sliderRect.width() * stage.scaler * musicPlayer.scaleX());
                mixer.instance.setPosition(mixer.instance.getDuration() * scaler);
            }
        });
        
        musicPlayer.add(sliderBackground);
        musicPlayer.add(sliderRect);

        musicPlayer.reset = function(){
            sliderRect.scaleX(0);
            musicPlayer.setStoped();
            mixer.instance.stop();
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.isAssigned = false;
        }

        return musicPlayer;

        function trackTime() {
            if(!musicPlayer.layerProcessed){
                musicPlayer.getLayer().drawScene.process();
                musicPlayer.layerProcessed = true;
            }

            musicPlayer.positionInterval = setInterval(function(event) {
                if(musicPlayer == null){
                    clearInterval(this);
                    return;
                }
                sliderRect.scaleX(mixer.instance.getPosition() / mixer.instance.getDuration());
                if(musicPlayer.drawSlider) musicPlayer.getLayer().drawScene();

            }, isMobile ? 100 : 30);
        }
    }*/

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                }

                outTween.play();
            });


            resultScreen.add(resultButton);

            resultLayer.draw();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        }

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        }

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        }

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

    function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

});