var colorSheme = {
	"body":{
		"backgroundColor": "#DCE6D1"
	},
	"scrollBar":{
		"trackColor": "#97C25A",
		"arrowColor": "#97C25A",
		"innerSliderColor": "#97C25A",
		"outerSliderColor": "#97C25A",
		"backgroundColor": "white"
	},
	"glowedWordColor": "#d32424",
	"glowedWord": {
		"regular": "#3059a5",
		"wrong": "#d32424",
		"hover": "#84b734",
		"stroke": "#648c57"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#97c25a",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"answerRect":{
		"color": "#97c25a",
		"stroke": "#97c25a",
		"cornerRadius": 2,
		"fontColor": "white"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"experimentPanel":{
		"backgroundColor": "white",
		"stroke": "#97c25a",
		"cornerRadius": 0,
		"opacity": 1
	},
	"experimentResult":{
		"backgroundColor": "white",
		"stroke": "#9DCD76"
	},
	"uiPanel":{
		"color": "#DCE6D1",
		"stroke": "#9DCD76"
	},
	"dropWord":{
		"backgroundColor": "#2E302A",
		"fontColor": "white",
		"stroke": "#82C351"
	},
	"defaultResult": "defaultResult.png",
	"header":{
		"sidePanel":{
			"color": "#82C351",
			"icon": "icon.png",
			"stroke": ""
		},
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a",
			"questionTip":{
				"backgroundColor":"#82C351",
				"fontColor": "white",
				"fontStroke": "grey"
			}
		}
	},
	"countDown":{
		"color": "#82C351"
	}
}