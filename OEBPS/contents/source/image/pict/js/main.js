$(function(){

	var config = location.href.split('[')[1].split(']')[0].split('|');

	var src = config[0],
		txt = decodeURI(config[1]),
		$wrap = $('#stageContainer'),
		$p = $('.dop_txt'),
		$i = $('.dop_img'),
		$border = $('.dop_border'),
		iW,
		iH,
		ratio,
		isWider,
		isImagePortrait,
		classes = '',
		$b = $('body');

	$i.attr('src', src);
	$p.html(txt);

	$i.load(function(){
		iW = $i.width();
		iH = $i.height();
		ratio = iW/iH;
		isImagePortrait = iH > iW;

		setClasses();

		$(window).on('resize', setClasses);
	});

	$p.load(setTimeout(function(){setDecor()}, 300));

	function checkWindow(ww,wh){
		isWider = ww / wh > ratio;
		isPortrait = ww < wh;
	}

	function setClasses(){
		$wrap.removeAttr('class');

		var ww = window.innerWidth, wh = window.innerHeight;

		if(ww < iW || wh < iH*1.3){

			$b.addClass('smallScreen');

			checkWindow(ww,wh);
			classes = ((isWider) ? 'nowider' : 'wider') + ' ' + ((isImagePortrait) ? 'imgPort' : 'imgLand');
			$wrap.addClass(classes);

		} else {
			$b.removeClass('smallScreen');
		}

		setDecor();
	}

	function setDecor(){

		var currImgW = $i.width(),
			currImgH = $i.height(),
			currPH = $p.height(),
			borderW = currImgW + 20,
			borderH = currImgH + currPH + 60;

		$p.width($i.width());
		$border.css({
			width: borderW + 'px',
			height: borderH + 'px',
			marginLeft: -borderW/2 + 'px'
		});
	}


});