$(window).load(function () {

    hookManager.applyFix('KitkatTouchFix');

    $(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

	Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    function reorient(e) {
        window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	/*Глобальные переменные**/

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    var isIOS = ( /|iPhone|iPad|iPod|/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';
	events[0].mousedown = 'mousedown';
	events[1].mousedown = 'tap';

	var activeRadioButton = null;

	var taskScreenArray = [];
	var currentTask = 0;

	var taskBoxArray = [];

	var taskDataCollection = [];
	var comment = '';

    var assetOqueue = [];

    var soundArray = [];

	var root = '';

    var taskData = [];

    var autoplay = true;

	var mixer = null;
	var titlePlayer = null;

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var aspectRatio = 0; //widescreen 16:9

	var defaultStageWidth = 1024;
	var defaultStageHeight = 576;

	var contentMargin = {x: 5, y: 5}

	var wrongCount = 0; 
	var correctCount = 0; 
	var missedCount = 0; 

	ANSWER_WRONG = -1;
	ANSWER_EMPTY = 0;
	ANSWER_CORRECT = 1;
	ANSWER_MISSED = 2;

	var spacer = 15;
	var marginTop = 30;
	var marginLeft = 50;

	var controllsScaler = 1.5

	var scrollBar;

    var colorSheme;

    var skinManager;

    var taskQuestion = null;

    var mainTitleRect = null;

    var levelManager = new LevelManager();

    var answerContainerPadding = 30;

    var taskPanel = null;

    var mainFont = 'mainFont';

    var scores = {
        wrong: 0,
        correct: 0,
        skipepd: 0
    }

    var playerProportions = {
        x: 250,
        y: 45
    }

	/*Слои**/
    var playerLayer = new Kinetic.Layer();
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var imageContainer = new Kinetic.Layer();
	var taskBoxLayer = new Kinetic.Layer();
	var contentScrollLayer = new Kinetic.Layer();

	var mainContainer = new Kinetic.Group({
        x:0,
        y:0
    });

    var contentContainer = new Kinetic.Group({
		x:0,
		y:0
	});

    contentLayer.add(mainContainer);
	mainContainer.add(contentContainer);

	stage.add(backgroundLayer);
    stage.add(playerLayer);
	stage.add(contentLayer);
	stage.add(taskBoxLayer);
	stage.add(contentScrollLayer);
	stage.add(imageContainer);

	var preLoader = null;

	$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(),  $(window).height());
	}

	function initAspectRatio(){
		if($(window).width() / $(window).height() < 1.6){
			aspectRatio = 1;
			defaultStageWidth = 1024;
			defaultStageHeight = 768;

            //if(isIOS) defaultStageWidth = 691;

			stage.width(defaultStageWidth);
			stage.width(defaultStageHeight);
		}
	}

	function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        stage.draw();
    }

	function initWindowSize(){
		scaleScreen(window.innerWidth, window.innerHeight);
	}

    skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(loadApp, 300);
    });

    function loadApp(){
        preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    }

    function xmlLoadHandler(xml){    	
    	initImageContainer();

    	root = $(xml).children('root');

        levelManager.initRoute(root);

    	taskDataCollection = root.children('task');

    	setTimeout(function(){
            preloadContent(taskDataCollection);
        }, 50);
    }

    function initApplication(){
    	contentLayer.defaultClip = {
		  x: 0,
		  y: 0,
		  width: $(window).width() / stage.scaler,
		  height: $(window).height() / stage.scaler - 90
		};

		contentLayer.extendedClip = {
		  x: 0,
		  y: 60,
		  width: contentLayer.defaultClip.width,
		  height: contentLayer.defaultClip.height
		};
    	
    	contentLayer.resetClip = function(){
			contentLayer.setClip(contentLayer.defaultClip);

			contentLayer.extended = false;
		}

		contentLayer.extend = function(){
			contentLayer.setClip(contentLayer.extendedClip);

			contentLayer.extended = true;
		}

		contentLayer.resetClip();

    	taskDataCollection = root.children('task');
    	comment = root.children('comment').text();

    	var mainTitle = root.children('title').text();

    	createHead(mainTitle, '');

    	var playerSize = aspectRatio == 1 ? 170 : 150;

    	if(mixer != null && typeof mixer != 'undefined'){
            titlePlayer = createMusicPlayer(55, 35, playerSize, playerSize, 0, mixer, autoplay);

        	playerLayer.add( titlePlayer );
            playerLayer.draw();
        }

    	if(taskDataCollection.length < 1) throw new Error('Wrong task count!!!');

    	showTask(0);

        buildTaskPanel();

    	/*for(var i = 0; i < taskDataCollection.length; i++){
    		createTaskBox(0 + 30 * i, ($(window).height() / stage.scaler) - 50 - screenMarginY, i);
    	}*/

    	scrollBar = createScrollBar($(window).width() / stage.scaler - 25, 70, 20, contentLayer.defaultClip.height - 10, contentContainer, contentLayer)
    	$(document).on("mousewheel", wheelScroll);

        scrollBar.refresh();

    	createButton((($(window).width() / stage.scaler) >> 1) - 70, ($(window).height() / stage.scaler) - 50, 140, 37, 'Ответить', answerButtonHandler);    	

    	preLoader.hidePreloader();
    }

    function preloadContent(dataCollection){

        var taskCounter = $(dataCollection).length;

        var imageLoadIndex = registerAssets();
        var soundLoadIndex = registerAssets();

    	$(dataCollection).each(function(index) {
            var data = $(this).children('data')
    		var sound = levelManager.getRoute($(data).attr('sound'));

            var imageLoadCounter = 0;

            var answers = $(data).children('answer');

            var text = $(data).children('text');

            taskData[index] = {
                answers: [],
                sound: false,
                controlls: calculateControllsType(answers),
                header: text.text(),
                image: null
            };

            var picture = levelManager.getRoute(text.attr('picture'));

            if(typeof picture != 'undefined' && picture != ''){
                imageLoadCounter++;

                taskData[index].image = new Image();
                taskData[index].image.onload = function(){
                    imageLoadCounter--;

                    if(imageLoadCounter == 0) taskImagesLoaded();
                }

                taskData[index].image.src = picture;
            }

            shuffle(answers);

            answers.each(function(aIndex){
                var image = $(this).attr('picture');
                var preloadedArray = [];

                taskData[index].answers[aIndex] = {
                    text: '',
                    image: null
                };

                taskData[index].answers[aIndex].text = $(this).text();
                taskData[index].answers[aIndex].image = null;
                taskData[index].answers[aIndex].value = $(this).attr('value');

                if(typeof image != 'undefined' && image != ''){
                    var imgObj = new Image();

                    imgObj.index = aIndex;
                    imgObj.text = $(this).text();

                    imageLoadCounter++;

                    imgObj.onload = function(){
                        var picture = new Kinetic.Image({
                            x: 0,
                            y: 0,
                            image: imgObj
                        });

                        taskData[index].answers[imgObj.index].text = imgObj.text;
                        taskData[index].answers[imgObj.index].image = picture;

                        picture.on(events[isMobile].click, imageZoomHandler)

                        imageLoadCounter--;

                        if(imageLoadCounter == 0) taskImagesLoaded();
                    }

                    imgObj.src = levelManager.getRoute($(this).attr('picture'));
                }
            })

            if(imageLoadCounter == 0) taskImagesLoaded();

    		if(typeof sound == 'undefined'){
                sound = '';
            } else{
               soundArray[index] = sound;
            }

            taskData[index].sound = (sound != '');
    	});

    	if(soundArray.length > 0){

	    	mixer = buildSoundMixer(soundArray, function(){
                setAssetLoaded(soundLoadIndex);

	    		//initApplication();

	            /*backgroundLayer.add( createMusicPlayer(100, 100, 120, 120, 0, mixer, true) );
	            backgroundLayer.draw();*/
	        });
	    } else{
	    	//initApplication();
            setAssetLoaded(soundLoadIndex);
	    }

        function imagesLoaded(){
            setAssetLoaded(imageLoadIndex);
        }
        function taskImagesLoaded(){
            taskCounter--;
            if(taskCounter == 0) imagesLoaded();
        }
    }

    function setAssetLoaded(assetIndex){
        assetOqueue[assetIndex] = true;
        return checkLoad();
    }

    function checkLoad(){
        var flag = true;
        for(var i = 0; i < assetOqueue.length; i++){
            if(!assetOqueue[i]) return false;
        }

        initApplication();

        return flag;
    }

    function registerAssets(){
        assetOqueue.push(false);
        return (assetOqueue.length - 1);
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height,
                scaleX: 1 / skinManager.getScaleFactor(),
                scaleY: 1 / skinManager.getScaleFactor()
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = skinManager.getGraphic('preLoader.png');

        return preloaderObject;
    }

    function buildSoundMixer(assetArray, onAssetsLoaded){
        var mixer = {};

        if (!createjs.Sound.initializeDefaultPlugins()) {return;}

        var audioPath = "";
        var manifest = [];

        for(var i = 0; i < assetArray.length; i++){
            if(assetArray[i] == '' || typeof assetArray[i] == 'undefined') continue;
            manifest.push({
                id: i,
                src: assetArray[i]
            });
        }

        if(manifest.length < 1){
            onAssetsLoaded();
            return;
        }
        
        mixer.loadCounter = manifest.length;

        createjs.Sound.alternateExtensions = ["ogg"];
        createjs.Sound.addEventListener("fileload", handleLoad);
        createjs.Sound.registerManifest(manifest, audioPath);

        mixer.targetUI = null;

        mixer.assignUI = function(ui){
            if(mixer.targetUI != null) mixer.targetUI.reset();
            mixer.targetUI = ui;
        }

        mixer.play = function(id){
            mixer.instance = createjs.Sound.play(id);
            mixer.instance.addEventListener ("complete", function(instance) {
                if(mixer.targetUI != null) mixer.targetUI.reset();
            });
        }

        function handleLoad(event) {
            mixer.loadCounter--;
            if(mixer.loadCounter == 0 && onAssetsLoaded) onAssetsLoaded()
        }

        return mixer;
    }

    function createMusicPlayer(x, y, maxWidth, maxHeight, musicID, mixer, autoplay){
        var playerSheme = colorSheme.player;

        var musicPlayer = new Kinetic.Group({
            x: x,
            y: y
        });

        if(maxWidth == 0) maxWidth = playerProportions.x;
        if(maxHeight == 0) maxHeight = playerProportions.y;

        if(isMobile) autoplay = false;

        var uiLoadCounter = 2;

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: playerProportions.x,
            height: playerProportions.y,
            fill: playerSheme.backgroundRectColor,
            //cornerRadius: 4
        });

        var scaler = calculateAspectRatioFit(backgroundRect.width(), backgroundRect.height(), maxWidth, maxHeight);

        musicPlayer.scale({
        	x: scaler,
        	y: scaler
        });

        backgroundRect.opacity(0.60);

        musicPlayer.add(backgroundRect);

        musicPlayer.isAssigned = false;

        musicPlayer.drawSlider = true;

        musicPlayer.controllsPadding = 10;

        musicPlayer.controllsSize = backgroundRect.height() - (musicPlayer.controllsPadding << 1);

        var playGraphic = new Image();

        var graphWidth = 20;
        var graphHeight = 26.5;

        playGraphic.onload = function(){
            var playButton = new Kinetic.Sprite({
                x: musicPlayer.controllsPadding,
                y: musicPlayer.controllsPadding,
                image: playGraphic,
                animation: autoplay ? 'pause' : 'play',
                animations: {
                    pause: [
                    // x, y, width, height
                      0, 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
                    ],
                    play: [
                      // x, y, width, height
                      graphWidth * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            /*playButton.scale({
                x: (1 / skinManager.getScaleFactor()),
                y: (1 / skinManager.getScaleFactor())
            });*/

            var playSize = {
                x: graphWidth * skinManager.getScaleFactor() - (musicPlayer.controllsPadding << 1),
                y: backgroundRect.height() - (musicPlayer.controllsPadding << 1)
            }

            playButton.scaler = calculateAspectRatioFit(graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor(), musicPlayer.controllsSize, musicPlayer.controllsSize);

            playButton.scale({
                x: playButton.scaler,
                y: playButton.scaler
            })

            musicPlayer.setStoped = function(){
                if(playButton.animation() != 'play'){
                    playButton.animation('play');
                    musicPlayer.getLayer().draw();
                    musicPlayer.paused = false;
                }
            }

            musicPlayer.assignToMixer = function(){
                mixer.assignUI(musicPlayer);
                musicPlayer.isAssigned = true;
                mixer.play(musicID);

                playButton.animation('pause');

                musicPlayer.paused = false;

                trackTime();
            }

            musicPlayer.play = function(newSoundID){
            	musicID = newSoundID;
            	musicPlayer.assignToMixer();
            }

            musicPlayer.add(playButton);
            musicPlayer.getLayer().draw();

            playButton.on(events[isMobile].mouseover, hoverPointer);
            playButton.on(events[isMobile].mouseout, resetPointer);

            playButton.on(events[isMobile].click, function(){
                if(!musicPlayer.isAssigned){
                    musicPlayer.assignToMixer();
                } else{
                
                    if(playButton.animation() == 'play'){
                        musicPlayer.paused ? mixer.instance.resume() : mixer.instance.play()

                        playButton.animation('pause');

                        musicPlayer.paused = false;
                    } else{
                       mixer.instance.pause();
                       playButton.animation('play');
                       musicPlayer.getLayer().draw();
                       musicPlayer.paused = true;
                    }
                }
            });


            //if(autoplay) musicPlayer.assignToMixer();

            uiLoadCounter--;
            if(uiLoadCounter == 0) onUILoaded();
        }

        musicPlayer.destroyPlayer = function(){
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.remove();
        }

        musicPlayer.isLoaded = false;

        musicPlayer.switchMusic = function(newSoundID){
            if(musicPlayer.isLoaded){
                musicPlayer.play(newSoundID);
            } else{
                musicPlayer.uiLoadCallback = function(){
                    musicPlayer.play(newSoundID);
                }
            }
        }

        musicPlayer.uiLoadCallback = null;

        playGraphic.src = skinManager.getGraphic('player/playButton.png');

        var stopGraphic = new Image();

        stopGraphic.onload = function(){

            var stopButtonSize = musicPlayer.controllsSize - 1;

            var stopButton = new Kinetic.Image({
                x: musicPlayer.controllsSize + musicPlayer.controllsPadding,
                y: musicPlayer.controllsPadding,
                image: stopGraphic,
                width: stopButtonSize,
                height: stopButtonSize
            });

            /*stopButton.scale({
                x: (1 / skinManager.getScaleFactor()),
                y: (1 / skinManager.getScaleFactor())
            })*/

            stopButton.on(events[isMobile].mouseover, hoverPointer);
            stopButton.on(events[isMobile].mouseout, resetPointer);

            stopButton.on(events[isMobile].click, function(){
                if(!musicPlayer.isAssigned) return;
                mixer.instance.stop();
                musicPlayer.setStoped();
            });

            musicPlayer.add(stopButton);
            musicPlayer.getLayer().draw();

            uiLoadCounter--;
            if(uiLoadCounter == 0) onUILoaded();
        }

        function onUILoaded(){
            if(autoplay){
                setTimeout(function(){
                    titlePlayer.switchMusic(currentTask);
                }, 200);
            } else{
                if(musicPlayer.uiLoadCallback){
                    musicPlayer.uiLoadCallback();
                    musicPlayer.uiLoadCallback = null;
                }
            }

            titlePlayer.isLoaded = true;
        }

        stopGraphic.src = skinManager.getGraphic('player/stopButton.png');

        var sliderRect = new Kinetic.Rect({
            x: 71,
            y: 18,
            width: backgroundRect.width() - 84,
            height: 7,
            fillLinearGradientStartPoint: {x: 0, y: 0},
            fillLinearGradientEndPoint: {x: backgroundRect.width() - 84, y: 7},
            fillLinearGradientColorStops: [0, playerSheme.trackColor[0], 1, playerSheme.trackColor[1]],
            scaleX: 0
        });

        var sliderBackground = new Kinetic.Rect({
            x: sliderRect.x(),
            y: sliderRect.y(),
            width: sliderRect.width(),
            height: sliderRect.height(),
            fill: playerSheme.trackBackroundColor
        });

        musicPlayer.instance = null;
        musicPlayer.currentTrack = '';

        musicPlayer.on(events[isMobile].click, function(){
            if(!musicPlayer.isAssigned) return;
            //if(musicPlayer.paused) mixer.instance.resume();
            var pointer = stage.getPointerPosition();
            var rectPos = sliderRect.getAbsolutePosition();

            var area = {
                left: rectPos.x,
                top: rectPos.y,
                right: rectPos.x + sliderRect.width() * stage.scaler * musicPlayer.scaleX(),
                bottom: rectPos.y + sliderRect.height() * stage.scaler * musicPlayer.scaleY()
            };

            if(pointer.x > area.left && pointer.x < area.right && pointer.y > area.top && pointer.y < area.bottom){
                var scaler = (pointer.x - area.left) / (sliderRect.width() * stage.scaler * musicPlayer.scaleX());
                mixer.instance.setPosition(mixer.instance.getDuration() * scaler);
            }
        });
        
        musicPlayer.add(sliderBackground);
        musicPlayer.add(sliderRect);

        musicPlayer.reset = function(){
            sliderRect.scaleX(0);
            musicPlayer.setStoped();
            mixer.instance.stop();
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.isAssigned = false;
        }

        return musicPlayer;

        function trackTime() {
            if(!musicPlayer.layerProcessed){
                musicPlayer.getLayer().drawScene.process();
                musicPlayer.layerProcessed = true;
            }

            musicPlayer.positionInterval = setInterval(function(event) {
                if(musicPlayer == null){
                    clearInterval(this);
                    return;
                }
                sliderRect.scaleX(mixer.instance.getPosition() / mixer.instance.getDuration());
                if(musicPlayer.drawSlider) musicPlayer.getLayer().drawScene();

            }, isMobile ? 100 : 30);
        }
    }

    function createHead(title, question){
        var headSheme = colorSheme.header;

        var padding = 10;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

		var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            fill: headSheme.mainTitle.fontColor,
            width: ($(window).width() / stage.scaler) - titleGroup.x() - 10,
            fontStyle: 'bold'
        });

		mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: $(window).width() / stage.scaler,
            height: mainTitle.height() + (padding << 1),
            stroke: headSheme.mainTitle.stroke
        })


		titleGroup.add(mainTitle);
        backgroundLayer.add(mainTitleRect);

        taskQuestion = createTaskQuestion(0, mainTitleRect.y() + mainTitleRect.height(), mainTitle.width());

        titleGroup.add(taskQuestion);

        backgroundLayer.add(titleGroup);

		backgroundLayer.draw();
	}

    function initImageContainer(){
    	var image = new Kinetic.Image({
		  x: 20,
		  y: 20
		});

		var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		var modalGroup = new Kinetic.Group();

		modalRect.opacity(0.8);

		image.on(events[isMobile].mouseover, hoverPointer);
    	image.on(events[isMobile].mouseout, resetPointer);

		image.hide();
		modalRect.hide();

		modalGroup.add(modalRect);
		modalGroup.add(image);

		imageContainer.add(modalGroup);

		imageContainer.zoomImage = function(imageObj){

            image.setImage(imageObj);

            var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight-40);

            image.width(imageObj.width * scaler);
            image.height(imageObj.height * scaler);

            if(!image.isVisible()){
                image.x(defaultStageWidth >> 1);
                image.y((defaultStageHeight - screenMarginY) >> 1);

                image.scale({
                    x: 0,
                    y: 0
                })

                var inTween = new Kinetic.Tween({
                    node: image,
                    scaleX: 1,
                    scaleY: 1,
                    x: (($(window).width() / stage.scaler) >> 1) - (image.width() >> 1),
                    y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (image.height() >> 1),
                    duration: 0.4,
                    easing: Kinetic.Easings.EaseInOut
                })
                inTween.onFinish = function(){
                    //modalRect.show();
                }

                modalRect.show();

                image.show();
                inTween.play();
            }

            modalGroup.on(events[isMobile].click, zoomOut);

            imageContainer.draw();
        }

		function zoomOut(evt){

	    	var outTween = new Kinetic.Tween({
	    		node: image,
	    		scaleX: 0,
	    		scaleY: 0,
	    		x: defaultStageWidth>>1,
	    		y: defaultStageHeight>>1,
				duration: 0.4,
				easing: Kinetic.Easings.EaseInOut
	    	});

	    	outTween.onFinish = function(){
		    	image.hide();
				imageContainer.draw();
			}

			modalRect.hide();

	    	outTween.play();
		}
    }

    function wheelScroll(evt){
    	scrollBar.scrollBy(-evt.originalEvent.wheelDelta/12);
    }

    function calculateControllsType(answers){
    	var trueCount = 0;
    	$(answers).each(function(){
    		if($(this).attr('value')=='1'){
    			trueCount++;
    			if(trueCount>1) return false;
    		}
    	});
    	return trueCount > 1 ? true : false;
    }

    function createTaskQuestion(x, y, width){
        var taskQuestionContainer = new Kinetic.Group({
            x: x,
            y: y
        });

        var headSheme = colorSheme.header;

        var questionPadding = 10;

        taskQuestionContainer.maxWidth = width - (questionPadding << 1);

        var taskQuestion = new Kinetic.Text({ //текстовая метка
            x: 35,
            y: questionPadding,
            text: '',
            fontSize: 18,
            fontFamily: mainFont,
            fill: headSheme.questionTitle.fontColor
        });

        var taskQuestionRect = new Kinetic.Rect({
            x: - x,
            y: 0,
            width: $(document).width() / stage.scaler,
            height: taskQuestion.height() + taskQuestion.fontSize() * 0.1 + (questionPadding << 1),
            fill: headSheme.questionTitle.backgroundColor,
            stroke: headSheme.questionTitle.stroke
            //cornerRadius: 15
        });

        var taskQuestionImage = new Kinetic.Image({
            x: 0,
            y: 0
        });

        taskQuestionImage.on(events[isMobile].click, function(){
            imageContainer.zoomImage(taskQuestionImage.getImage());
        });

        if(!isMobile){
            taskQuestionImage.on(events[isMobile].mouseover, hoverPointer);
            taskQuestionImage.on(events[isMobile].mouseout, resetPointer);
        }

        taskQuestionContainer.setScore = function(correctCount, wrongCount, missedCount){
            taskQuestionContainer.setValue('Правильных ответов - '+correctCount+'.' + 
                    '\nНеправильных ответов - '+wrongCount+'.' +
                    '\nПропущенных ответов - '+missedCount+'.')
        }

        taskQuestionContainer.setValue = function(text, showPlayer, image){
            if(text == '' && !showPlayer && !image){
                taskQuestionContainer.hide();
                return;
            }

            if(!taskQuestionContainer.isVisible()) taskQuestionContainer.show();

            taskQuestion.text(text);

            if(taskQuestion.width() > taskQuestionContainer.maxWidth) taskQuestion.width(taskQuestionContainer.maxWidth);

            var newHeight = taskQuestion.height() + questionPadding;

            if(image){
                var scaler = calculateAspectRatioFit(image.width, image.height, 100, 100);

                taskQuestionImage.setImage(image);

                taskQuestionImage.scale({
                    x: scaler,
                    y: scaler
                })

                taskQuestionImage.x(taskQuestionContainer.getParent().x() + 35);
                taskQuestionImage.y(taskQuestion.y() + newHeight);

                newHeight += image.height * scaler + (questionPadding >> 1);
                taskQuestionImage.show();
            } else{
                taskQuestionImage.hide();
            }

            if(showPlayer && titlePlayer){
                titlePlayer.show();
                titlePlayer.x(taskQuestionContainer.getParent().x() + 35);
                titlePlayer.y(taskQuestion.y() + taskQuestionContainer.y() + newHeight);

                newHeight += 35;
            }

            newHeight += questionPadding

            playerLayer.draw();

            taskQuestionRect.height(newHeight);

            mainTitleRect.height(taskQuestionContainer.y() + newHeight);
        }

        taskQuestionContainer.add(taskQuestionRect);
        taskQuestionContainer.add(taskQuestion);
        taskQuestionContainer.add(taskQuestionImage);

        return taskQuestionContainer;
    }

    function updateClip(){
        var defaultClip = contentLayer.defaultClip;
        var newClip = {};

        newClip.x = defaultClip.x;
        newClip.y = mainTitleRect.height() + 5;
        newClip.height = defaultClip.height - mainTitleRect.height() + 5;
        newClip.width = defaultClip.width;

        contentLayer.setClip(newClip);

        mainContainer.y(newClip.y + 20);
    }

    function showTask(taskIndex){
    	var margin = 0;
    	var contentWidth = contentLayer.getClip().width - contentLayer.getClip().x;

        var maxContentWidth = contentWidth;
        var taskWidth = 0;

        var controllsPadding = 30;

        var taskScheme = skinManager.colorSheme.taskContainer;

        var showPlayer = false;

        if(mixer != null){

            var sound = taskData[taskIndex].sound;

            if(!sound){
                if(titlePlayer.isVisible()){
                    titlePlayer.hide();
                }
            } else{
                showPlayer = true;

                if(!titlePlayer.isVisible()){
                    titlePlayer.show();
                }

                if(taskIndex != 0) setTimeout(titlePlayer.switchMusic(taskIndex), 400);
            }
        }

        taskQuestion.setValue((currentTask + 1) + '. ' + taskData[taskIndex].header, showPlayer, taskData[taskIndex].image);
        backgroundLayer.draw();

        updateClip();

        margin = 0;

        var answerSheme = colorSheme.answerContainer;

    	taskScreenArray[currentTask] = [];
    	taskScreenArray[currentTask]['controlls'] = [];

        var contentClip = contentLayer.getClip();

    	var container = new Kinetic.Group({
    		x: answerContainerPadding + 5,
    		y: 0
    	});

        container.defaultX = container.x();
        container.defaultY = container.y();

        var taskRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: $(document).width() / stage.scaler - answerContainerPadding - 25,
            height: contentLayer.getClip().height,
            stroke: taskScheme.outerRect.strokeColor,
            fill: taskScheme.outerRect.backgroundColor,
            strokeWidth: 1
        });

    	contentContainer.add(container);

        container.add(taskRect);

        var taskContentContainer = new Kinetic.Group({
            x: 0,
            y: 0
        });

        container.add(taskContentContainer);

        taskScreenArray[currentTask]['container'] = container;
    	taskScreenArray[currentTask].container['taskContentContainer'] = taskContentContainer;

        taskRect.hide();

        container.taskRect = taskRect;

        var answerRectContainer = new Kinetic.Group({
            x: 0,
            y: 0
        });

        container.add(answerRectContainer);

    	var header = new Kinetic.Text({ //текстовая метка
	        x: marginLeft,
	        y: 10 + margin,
	        text: (currentTask + 1) + '. ' + taskData[taskIndex].header,
	        fontSize: 18,
	        fontFamily: mainFont,
	        fill: 'black',
            width: maxContentWidth
	      });

    	var taskHeigth = 0;

    	var answerLabel;

    	var maxPicSize = 200;
    	var answerMargin = 30;

    	var isCheckControlls = taskData[taskIndex].controlls;

        var answerArray = taskData[taskIndex].answers;

        var currentScaler = 1;

        var picSize = {
            x: 0,
            y: 0
        }

        var dy = 0;

        var positionArray = [];

        var answerMaxWidth = ($(document).width() / stage.scaler - (answerContainerPadding << 1));
        var answer;

        var answerRectPadding = 10;

        var controlls = null;

        container.hovered = null;
        container.selected = null;

        container.hover = function(id){
            if(container.hovered == id) return;
            if(container.isClear) return;
            if(container.hovered != null){
                if(container.selected != container.hovered){
                    container.rectCollection[container.hovered].fill(taskScheme.innerRect.backgroundColor);
                    container.rectCollection[container.hovered].stroke(taskScheme.innerRect.strokeColor);
                }
            }
            container.hovered = id;
            container.rectCollection[id].fill(taskScheme.selected.backgroundColor);
            container.rectCollection[id].stroke(taskScheme.selected.strokeColor);

            contentLayer.batchDraw();
        }

        container.unHover = function(){
            if(container.isClear) return;
            if(container.hovered != null){
                if(container.selected != container.hovered){
                    container.rectCollection[container.hovered].fill(taskScheme.innerRect.backgroundColor);
                    container.rectCollection[container.hovered].stroke(taskScheme.innerRect.strokeColor);

                    container.hovered = null;
                }
            }

            contentLayer.batchDraw();
        }

        container.select = function(id){
            if(container.selected == id) return;
            if(container.isClear) return;
            if(container.selected != null){
                container.rectCollection[container.selected].fill(taskScheme.innerRect.backgroundColor);
                container.rectCollection[container.selected].stroke(taskScheme.innerRect.strokeColor);
            }

            container.selected = id;
            container.rectCollection[id].fill(taskScheme.selected.backgroundColor);
            container.rectCollection[id].stroke(taskScheme.selected.strokeColor)

            contentLayer.batchDraw();
        }

        container.isClear = false;

        container.setClear = function(){
            for(var i = 0; i < container.rectCollection.length; i++){
                container.rectCollection[i].hide();
            }

            container.isClear = true;

            container.taskRect.show();
        }

        container.rectCollection = [];

        for(var i = 0; i < answerArray.length; i++){
            var value = taskData[taskIndex].answers[i].value;
            
            answer = createAnswerItem(30, dy, taskData[taskIndex].answers[i], answerMaxWidth, i);

            if(!isCheckControlls){
                controlls = createRadioButton(0, dy, value, answerRectContainer, i);
            } else{
                controlls = createCheckBox(0, dy, value, answerRectContainer, i);
            }

            answerRect = new Kinetic.Rect({
                x:  - answerRectPadding,
                y: dy - answerRectPadding,
                width: answerMaxWidth,
                height: answer.height() + (answerRectPadding << 1),
                stroke: taskScheme.innerRect.strokeColor,
                fill: taskScheme.innerRect.backgroundColor,
                strokeWidth: 1
            });

            answer.controlls = controlls;
            answerRect.controlls = controlls;
            answerRect.id = i;

            container.rectCollection[i] = answerRect;

            if(!isMobile){
                answerRect.on(events[isMobile].mouseover, function(){
                    this.controlls.hover();
                    container.hover(this.id);
                });
                answerRect.on(events[isMobile].mouseout, function(){
                    this.controlls.unHover()
                    container.unHover();
                });
            }

            answerRect.on(events[isMobile].click, function(){
                this.controlls.check();
            });

            answerRectContainer.add(answerRect);
            taskContentContainer.add(answer);

            dy += answer.height() + answerMargin;
        }

        taskContentContainer.moveToTop();

        taskHeigth = dy - answerMargin;

        var answerRect;

    	//container.height(taskHeigth);
    	//if(contentContainer.height()<taskHeigth) contentContainer.height(taskHeigth);

        var taskRectPadding = 18;

        taskRect.x(taskRect.x() - taskRectPadding);
        taskRect.y(taskRect.y() - taskRectPadding);

        //taskRect.width(taskWidth + answerMargin + (taskRectPadding << 1));
        taskRect.height(taskHeigth + taskRectPadding + 20);

        container.height(taskRect.height());
        contentContainer.height(taskRect.height());

        //contentContainer.height(taskHeigth + 50 + (taskRectPadding << 1));

        //container.height(taskHeigth + 50 + (taskRectPadding << 1));

    	//TODO: UPDATE SCROLLBAR HEIGTH!!!

        if(scrollBar != null) scrollBar.refresh();

    	//Tween Section

    	container.x(defaultStageWidth >> 1);
    	container.y(defaultStageHeight >> 1);

    	container.scale({
    		x: 0,
    		y: 0
    	})

    	var inTween = new Kinetic.Tween({
    		node: container,
    		scaleX: 1,
    		scaleY: 1,
    		x: container.defaultX,
    		y: container.defaultY,
			duration: 0.6,
			easing: Kinetic.Easings.EaseInOut,
            onFinish: function(){
                setTimeout(function(){
                    if(titlePlayer) titlePlayer.drawSlider = true;
                }, 1000);
            }
    	});

    	inTween.play();
    }

    function createAnswerItem(x, y, answerData, maxWidth, id){
        var answerGroup = new Kinetic.Group({
            x: x,
            y: y
        });

        answerGroup.id = id;

        answerGroup.onHover = function(){
            if(!answerGroup.controlls) return;
            answerGroup.controlls.hover();
        }

        answerGroup.onUnHover = function(){
            if(!answerGroup.controlls) return;
            answerGroup.controlls.unHover();
        }

        answerGroup.onCheck = function(){
            if(!answerGroup.controlls) return;
            answerGroup.controlls.check();
        }

        var text = answerData.text;
        var picture = answerData.image;

        var spacer = 5;
        var maxPicSize = 180;

        var labelMargin = 0;

        if(text!=''){
            var label = createLabel(0, 0, text, maxWidth);

            if(label.width() < maxWidth){
                //label.x((maxWidth >> 1) - (label.width() >> 1));
            }

            answerGroup.add(label);

            answerGroup.height(label.height())

            labelMargin += answerGroup.height() + spacer;

            if(!isMobile){
                label.on(events[isMobile].mouseover, answerGroup.onHover);
                label.on(events[isMobile].mouseout, answerGroup.onUnHover);
            }

            label.on(events[isMobile].click, answerGroup.onCheck);
        }

        if(picture != null){
            var scaler = calculateAspectRatioFit(picture.width(), picture.height(), maxPicSize, maxPicSize);

            picture.scale({
                x: scaler,
                y: scaler
            });

            var picWidth = picture.width() * scaler
            var picHeight = picture.height() * scaler

            if(picWidth < maxWidth){
                //picture.x((maxWidth >> 1) - (picWidth >> 1));
            }

            picture.y(labelMargin);

            answerGroup.height(answerGroup.height() + picHeight)

            answerGroup.add(picture);
        }

        return answerGroup;
    }

    function answerButtonHandler(evt){
    	var controllsArray = taskScreenArray[currentTask].controlls;
    	var answerFlag;

    	var currentWrongCount = 0;
    	var currentCorrectCount = 0;
    	var currentMissedCount = 0;


    	var prevTask = taskScreenArray[currentTask].container

    	var outTween = new Kinetic.Tween({
    		node: prevTask,
    		scaleX: 0,
    		scaleY: 0,
    		x: defaultStageWidth >> 1,
    		y: defaultStageHeight >> 1,
			duration: 0.6,
			easing: Kinetic.Easings.EaseInOut
    	});

        if(titlePlayer) titlePlayer.drawSlider = false;

    	outTween.onFinish = function(){
    		prevTask.hide();
    		prevTask.scale({
    			x: 1,
    			y: 1
    		});

    		prevTask.x(0);
    		prevTask.y(0);
    	}
    	

    	//taskScreenArray[currentTask].container.hide();
    	scrollBar.resetScroll();

    	for (var i = 0; i < controllsArray.length; i++) {

    		answerScore = controllsArray[i].checkValue();
    		controllsArray[i].enable = false;

    		switch(answerScore){
	    		case ANSWER_WRONG: currentWrongCount++;
	    			break;
	    		case ANSWER_CORRECT: currentCorrectCount++;
	    			break;
	    		case ANSWER_MISSED: currentMissedCount++;
	    			break;
	    	}
    	}    	

    	wrongCount += currentWrongCount;
    	correctCount += currentCorrectCount;
    	missedCount += currentMissedCount;

    	if(currentCorrectCount != 0){
			if(currentWrongCount != 0 || currentMissedCount != 0){
				//taskBoxArray[currentTask].setComposite();
                taskPanel.setCurrentState('composite');
                scores.wrong++;
			} else{
				//taskBoxArray[currentTask].setCorrect();
                taskPanel.setCurrentState('correct');
                scores.correct++;
			}
		} else{
			if(currentWrongCount != 0){
				//taskBoxArray[currentTask].setWrong();
                taskPanel.setCurrentState('wrong');
                scores.wrong++;
			} else{
				//taskBoxArray[currentTask].setMissed();
                taskPanel.setCurrentState('missed');
                scores.skipepd++;
			}
		}

		//contentLayer.draw();

    	if(currentTask != (taskDataCollection.length-1)){

    		currentTask++;

    		//taskBoxArray[currentTask].setCurrent();
            taskPanel.moveTo(currentTask);

            taskBoxLayer.drawScene();

    		showTask(currentTask);
    	} else{ //Score Screen
    			if(mixer != null){
	    			contentLayer.resetClip();
	    			titlePlayer.destroyPlayer();
	    			mixer.instance.stop();
                    playerLayer.draw();
	    		}

                taskBoxLayer.drawScene();

    			outTween.duration = 0.2;

		    	outTween.onFinish = function(){
                    prevTask.hide();

                    prevTask.scale({
                        x: 1,
                        y: 1
                    });

                    prevTask.x(0);
                    prevTask.y(0);

		    		var dy = 0;

		    		var scoreString = 'Правильных ответов - '+correctCount+'.';
		    		scoreString += '\nНеправильных ответов - '+wrongCount+'.';
		    		scoreString += '\nПропущенных ответов - '+missedCount+'.';

		    		/*var scoreLabel = new Kinetic.Text({ //текстовая метка
				        x: marginLeft,
				        y: marginTop + 10,
				        text: scoreString,
				        height: 100,
				        fontSize: 18,
				        fontFamily: mainFont,
				        fill: 'black'
				      });*/

				    //contentContainer.add(scoreLabel);

		    		if(comment!=''){
					    var commentLabel = new Kinetic.Text({ //текстовая метка
					        x: 0,
					        y: 0,
					        text: comment,
					        fontSize: 18,
					        fontFamily: mainFont,
					        fill: 'black'
					      });
					}

                    //taskQuestion.setValue(scoreString + (comment == '' ? '' : ('\n' + comment)), false);
                    //taskQuestion.setScore(correctCount, wrongCount, missedCount);
                    taskQuestion.setScore(scores.correct, scores.wrong, scores.skipepd);

                    updateClip();

					var totalHeight = 0;

                    //if(!titlePlayer) dy+=30

                    dy += 8;

                    if(comment != ''){
                        contentContainer.add(commentLabel);
                        dy += commentLabel.height() + 30;
                    }

                    var resultPadding = 15;
		    		
		    		for(i = 0; i<taskScreenArray.length; i++){
                        taskScreenArray[i].container.setClear();
		    			totalHeight = dy;
		    			taskScreenArray[i].container.y(dy);
		    			taskScreenArray[i].container.show();
                        dy += taskScreenArray[i].container.height() + resultPadding;
		    		}    		
                    contentContainer.height(dy);

		    		var button = evt.targetNode.getParent();
			    	button.setText('Ещё раз');
			    	button.off(events[isMobile].click);
			    	button.on(events[isMobile].click, clear);

		    		contentContainer.scale({
			    		x: 0,
			    		y: 0
			    	});

                    var contentClip = contentLayer.getClip();

			    	contentContainer.x(defaultStageWidth >> 1);
			    	contentContainer.y(defaultStageWidth >> 1);

			    	var inTween = new Kinetic.Tween({
			    		node: contentContainer,
			    		scaleX: 1,
			    		scaleY: 1,
			    		x: 35,
			    		y: 0,
						duration: 0.6,
						easing: Kinetic.Easings.EaseInOut,
                        onFinish: function(){
                            scrollBar.refresh();
                        }
			    	})

			    	inTween.play();
		    	}
    	}

    	outTween.play();
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function createLabel(x, y, text, maxWidth){
    	answerLabel = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: text,
	        fontSize: 18,
	        fontFamily: mainFont,
	        fill: 'black'
	      });

        if(answerLabel.width() > maxWidth){
            answerLabel.width(maxWidth);
        }

		//taskScreenArray[currentTask].container.taskContentContainer.add(answerLabel);
		//contentLayer.draw();

		return answerLabel;
    }
    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

	    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

	    return ratio;
	 }
    function createImage(x, y, image, scaler){
        image.x(x);
        image.y(y);

        //var scaler = calculateAspectRatioFit(image.width(), image.height(), maxSize, maxSize);

        /*image.scaleX(scaler);
        image.scaleY(scaler);
        image.scaler = scaler;*/

        image.defaultPositionX = image.x();
        image.defaultPositionY = image.y();

        image.on(events[isMobile].click, imageZoomHandler);
        image.on(events[isMobile].mouseover, hoverPointer);
        image.on(events[isMobile].mouseout, resetPointer);

        image.isZoomed = false;

        taskScreenArray[currentTask].container.taskContentContainer.add(image);


    	/*var imgObj = new Image();

		imgObj.onload = function(){
			var image = new Kinetic.Image({
			  x: x,
			  y: y,
			  image: imgObj,
			  width: imgObj.width,
			  height: imgObj.height
			});

			var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, maxSize, maxSize);

			image.scaleX(scaler);
			image.scaleY(scaler);
			image.scaler = scaler;

			image.defaultPositionX = image.x();
			image.defaultPositionY = image.y();

			image.on(events[isMobile].click, imageZoomHandler);
			image.on(events[isMobile].mouseover, hoverPointer);
			image.on(events[isMobile].mouseout, resetPointer);

			image.isZoomed = false;

			taskScreenArray[currentTask].container.add(image);

			contentLayer.draw();
		};
		imgObj.src = picture;.
        */
    }

    /*Скалирует изображения по клику*/
    function imageZoomHandler(evt){
        var currentImg = evt.targetNode;
        imageContainer.zoomImage(currentImg.getImage());
    }

    function createButton(x, y, width, height, textLabel, callback){
        var buttonSheme = colorSheme.button;

    	var buttonGroup = new Kinetic.Group({ x:x, y:y });
    	var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
	        x: 0,
	        y: 0,
	        width: width,
	        height: height,
	        fillLinearGradientStartPoint: {x: width >> 1, y: 0},
	        fillLinearGradientEndPoint: {x: width >> 1, y: height},
	        fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
	        stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
        });

    	var label = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: 0,
	        width: buttonRectangle.width(),
	        text: textLabel,
	        fontSize: 24,
	        fontFamily: mainFont,
	        fill: buttonSheme.labelColor,
	        align: 'center'
	      });

    	var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

    	if(dy > 0) label.y(dy);

    	label.height(buttonRectangle.height());
    	/*Определяет новую текстовую метку кнопки
    	@param {string} value новое значение метки**/
    	buttonGroup.setText = function(value){
    		label.text(value);
    		backgroundLayer.draw();
    	};

    	buttonGroup.add(buttonRectangle);
    	buttonGroup.add(label);
    	backgroundLayer.add(buttonGroup);
    	backgroundLayer.draw();
    	
    	buttonGroup.on(events[isMobile].click, callback);
    	buttonGroup.on(events[isMobile].mouseover, hoverPointer);
    	buttonGroup.on(events[isMobile].mouseout, resetPointer);

		return buttonGroup;
    }
    function createRadioButton(x, y, answerValue, container, id){
    	var checkBoxGroup = new Kinetic.Group({
    		x: x,
    		y: y - 2
    	});

        checkBoxGroup.id = id;

        container.add(checkBoxGroup);

    	var imageObj = new Image();

        var graphSize = 21;

        imageObj.onload = function() {
			var checkBox = new Kinetic.Sprite({
				x: 0,
				y: 0,
				image: imageObj,
				animation: 'clear',
				animations: {
					clear: [
					  // x, y, width, height
					  0,0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					hover: [
					  // x, y, width, height
					  graphSize * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					active: [
					  // x, y, width, height
					  graphSize * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					correct: [
					  // x, y, width, height
					  (graphSize << 1) * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					wrong: [
					  // x, y, width, height
					  (graphSize * 3) * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					missed: [
					  // x, y, width, height
					  (graphSize << 2) * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					]
				},
				frameRate: 7,
				frameIndex: 0,
                scale: {
                    x: 1 / skinManager.getScaleFactor(),
                    y: 1 / skinManager.getScaleFactor()
                }
			});

			checkBox.value = 0;
			checkBox.enable = true;

			var hitCircle = new Kinetic.Circle({
				x: 8,
				y: 8,
				fill: 'black',
				radius: 11 * controllsScaler
			});

			hitCircle.opacity(0);

			checkBoxGroup.add(checkBox);
			checkBoxGroup.add(hitCircle);

            checkBoxGroup.moveToTop();

            //taskScreenArray[currentTask].container.taskContentContainer.add(checkBoxGroup);

            /*checkBoxGroup.y(checkBoxGroup.y() + taskScreenArray[currentTask].container.taskContentContainer.y());

			taskScreenArray[currentTask].container.add(checkBoxGroup);
			contentLayer.draw();*/

            checkBox.isBusy = false;

			checkBox.checkValue = function(){
                checkBox.isBusy = true;
				if(answerValue == 1){
					if(answerValue==checkBox.value){
						checkBox.setCorrect();
						return ANSWER_CORRECT;
					} else{
						checkBox.setMissed();
						return ANSWER_MISSED;
					}
				} else if(checkBox.value == 1){
					checkBox.setWrong();
					return ANSWER_WRONG;
				}

				return ANSWER_EMPTY;
			}

			checkBox.setMissed = function(){
				checkBox.animation('missed');
				//contentLayer.draw();
			}

			checkBox.setWrong = function(){
				checkBox.animation('wrong');
				//contentLayer.draw();
			}
			checkBox.setCorrect = function(){
				checkBox.animation('correct');
				//contentLayer.draw();
			}

			checkBox.clear = function(){
                if(checkBox.isBusy) return;
				checkBox.animation('clear');
				//contentLayer.draw();
				checkBox.value = 0;
			}

            checkBoxGroup.hover = function(){
                if(checkBox.animation()!='active'&&checkBox.enable){
                    checkBox.animation('hover');
                    hoverPointer();
                    contentLayer.draw();
                }
            }

            checkBoxGroup.unHover = function(){
                if(checkBox.animation()!='active'&&checkBox.enable){
                    checkBox.animation('clear');
                    resetPointer();
                    contentLayer.draw();
                }
            }

            checkBoxGroup.check = function(){
                if(checkBox.enable){
                    if(activeRadioButton!=null) activeRadioButton.clear();
                    checkBox.animation('active');
                    contentLayer.draw();
                    checkBox.value = 1;
                    activeRadioButton = checkBox;
                    taskScreenArray[currentTask].container.select(checkBoxGroup.id);
                }
            }

            if(!isMobile){
    			hitCircle.on(events[isMobile].mouseover, function(evt){
    				checkBoxGroup.hover();
    			});
    			hitCircle.on(events[isMobile].mouseout, function(evt){
    				checkBoxGroup.unHover();
    			});
            }

			hitCircle.on(events[isMobile].click, function(evt){
				checkBoxGroup.check();
			});

			taskScreenArray[currentTask].controlls.push(checkBox);
		}
		imageObj.src = skinManager.getGraphic(colorSheme.radioButtonGraphic);

        return checkBoxGroup;
    }

    function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();

        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                }
            }
          });

          var scrollCircle = new Kinetic.Group();

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 10,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
            scrollTopArrowGroup.hide();
            scrollBotArrowGroup.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
            scrollTopArrowGroup.show();
            scrollBotArrowGroup.show();
          }

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            vscrollArea.y(clip.y + 10);
            vscrollArea.height(clip.height - 10);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(clip.y + 10);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            //contentGroup.y(0);
            vscroll.y(scrollStart);

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          }

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }

    function createCheckBox(x, y, answerValue, container, id){
    	var checkBoxGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

        checkBoxGroup.id = id;

        container.add(checkBoxGroup);

    	var imageObj = new Image();

        var graphSize = 22;

        imageObj.onload = function() {
			var checkBox = new Kinetic.Sprite({
				x: 0,
				y: - 2,
				image: imageObj,
				animation: 'clear',
				animations: {
					clear: [
					  // x, y, width, height
					  0,0,graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					hover: [
					  // x, y, width, height
					  graphSize * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					active: [
					  // x, y, width, height
					  graphSize * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					correct: [
					  // x, y, width, height
					  (graphSize << 1) * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(),graphSize * skinManager.getScaleFactor()
					],
					wrong: [
					  // x, y, width, height
					  (graphSize * 3) * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					],
					missed: [
					  // x, y, width, height
					  (graphSize << 2) * skinManager.getScaleFactor(), 0, graphSize * skinManager.getScaleFactor(), graphSize * skinManager.getScaleFactor()
					]
				},
				frameRate: 7,
				frameIndex: 0
			});
			checkBox.value = 0;
			checkBox.enable = true;

			checkBox.scale({
				x: 1 / skinManager.getScaleFactor(),
				y: 1 / skinManager.getScaleFactor()
			})

			var hitCircle = new Kinetic.Circle({
				x: 8,
				y: 8,
				fill: 'black',
				radius: 11 * controllsScaler
			});

			hitCircle.opacity(0);

			checkBoxGroup.add(checkBox);
			checkBoxGroup.add(hitCircle);

            checkBoxGroup.moveToTop();

			//taskScreenArray[currentTask].container.taskContentContainer.add(checkBoxGroup);
			//contentLayer.draw();

			checkBox.checkValue = function(){
				if(answerValue == 1){
					if(answerValue == checkBox.value){
						checkBox.setCorrect();
						return ANSWER_CORRECT;
					} else{
						checkBox.setMissed();
						return ANSWER_MISSED;
					}
				} else if(checkBox.value == 1){
					checkBox.setWrong();
					return ANSWER_WRONG;
				}

				return ANSWER_EMPTY;
			}

            checkBox.busy = false;

			checkBox.setMissed = function(){
				checkBox.animation('missed');
				//contentLayer.draw();
                checkBox.busy = true;
			}

			checkBox.setWrong = function(){
				checkBox.animation('wrong');
				//contentLayer.draw();
                checkBox.busy = true;
			}

			checkBox.setCorrect = function(){
                checkBox.animation('correct');
                //contentLayer.draw();
                checkBox.busy = true;
            }

            checkBox.setClear = function(){
				checkBox.animation('clear');
				//contentLayer.draw();
                checkBox.busy = true;
			}

			checkBoxGroup.hover = function(){
                if(checkBox.animation()!='active'&&checkBox.enable){
                    checkBox.animation('hover');
                    hoverPointer();
                    contentLayer.draw();
                }
            }

            checkBoxGroup.unHover = function(){
                if(checkBox.animation()!='active'&&checkBox.enable){
                    checkBox.animation('clear');
                    resetPointer();
                    contentLayer.draw();
                }
            }

            checkBoxGroup.check = function(){
                if(checkBox.enable){
                    if(checkBox.value != 1){
                        checkBox.animation('active');
                        contentLayer.batchDraw();
                        checkBox.value = 1;
                    } else{
                        checkBox.animation('clear');
                        contentLayer.batchDraw();
                        checkBox.value = 0;
                    }
                }
            }

            if(!isMobile){
                hitCircle.on(events[isMobile].mouseover, function(evt){
                    checkBoxGroup.hover();
                });
                hitCircle.on(events[isMobile].mouseout, function(evt){
                    checkBoxGroup.unHover();
                });
            }

            hitCircle.on(events[isMobile].click, function(evt){
                checkBoxGroup.check();
            });

			taskScreenArray[currentTask].controlls.push(checkBox);
		}
		imageObj.src = skinManager.getGraphic(colorSheme.checkBoxGraphic);

        return checkBoxGroup;
    }

    function createTaskBox(x, y, index){
    	var imageObj = new Image();

        var taskBoxGroup = new Kinetic.Group({
            x: x,
            y: y
        });

        imageObj.onload = function() {
        	var boxLabel = new Kinetic.Text({ //текстовая метка
		        x: (index > 8 ? 2 : 9),
		        y: 5,
		        text: (index + 1),
		        fontSize: 18,
		        fontFamily: mainFont,
		        fill: 'black'
		      });

            var graphWidth = 27;
            var graphHeight = 27;

			var taskBox = new Kinetic.Sprite({
				x: 0,
				y: 0,
				image: imageObj,
				animation: 'empty',
				animations: {
					empty: [
					  // x, y, width, height
					  0, 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
					],
					current: [
					  // x, y, width, height
					  graphWidth * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
					],
					correct: [
					  // x, y, width, height
					  (graphWidth << 1) * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
					],
					wrong: [
					  // x, y, width, height
					  (graphWidth * 3) * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
					],
					missed: [
					  // x, y, width, height
					  (graphWidth << 2) * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
					],
					composite: [
					  // x, y, width, height
					  (graphWidth * 5) * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
					]
				},
				frameRate: 7,
				frameIndex: 0			
			});

			taskBox.scale({
				x: 1 / skinManager.getScaleFactor(),
				y: 1 / skinManager.getScaleFactor()
			})

			taskBoxGroup.setEmpty = function(){
				taskBox.animation('empty');
				//taskBoxLayer.draw();
			}
			taskBoxGroup.setCurrent = function(){
				taskBox.animation('current');
				//taskBoxLayer.draw();
			}
			taskBoxGroup.setCorrect = function(){
				taskBox.animation('correct');
				//taskBoxLayer.draw();
			}
			taskBoxGroup.setWrong = function(){
				taskBox.animation('wrong');
				//taskBoxLayer.draw();
			}
			taskBoxGroup.setMissed = function(){
				taskBox.animation('missed');
				//taskBoxLayer.draw();
			}
			taskBoxGroup.setComposite = function(){
				taskBox.animation('composite');
				//taskBoxLayer.draw();
			}

            taskBoxGroup.setState = function(state){
                taskBox.animation(state);
            }

            taskBoxGroup.getState = function(){
                return taskBox.animation();
            }

            taskBoxGroup.getLabel = function(){
                return boxLabel.text();
            }

            taskBoxGroup.setLabel = function(value){
                boxLabel.x(value > 9 ? 4 : 9);
                boxLabel.text(value);
            }

			if(index == 0) taskBox.animation('current');

			taskBoxGroup.add(taskBox);
			taskBoxGroup.add(boxLabel);

            taskBoxGroup.getParent().setLoaded();

			taskBoxArray[index] = taskBoxGroup;
		}

		imageObj.src = skinManager.getGraphic(colorSheme.taskBoxGraphic);

        return taskBoxGroup;
    }

    function buildTaskPanel(){
        taskPanel = new Kinetic.Group({
            x: 20,
            y: ($(window).height() / stage.scaler) - 50
        });

        var taskCounter = new Kinetic.Text({
            x: ($(window).width() / stage.scaler) - 100,
            y: 15,
            text: '1 из ' + taskDataCollection.length,
            fontFamily: mainFont,
            fontSize: 20,
            fill: 'black'
        });

        taskPanel.setCurrentState = function(state){
            taskBoxArray[taskPanel.current].setState(state);
        }

        taskPanel.moveTo = function(index){
            taskCounter.text((index + 1) + ' из ' + taskDataCollection.length);
            if(index <= 4){
                taskBoxArray[index].setCurrent();
                taskPanel.current++;
            } else{
                var prevState = '';
                var prevLabel = '';

                var currentState = '';
                var currentLabel = '';

                for(var i = 4; i >= 0; i--){
                    if(i == 4){
                        prevState = taskBoxArray[i].getState();
                        prevLabel = taskBoxArray[i].getLabel();

                        taskBoxArray[i].setCurrent();
                        taskBoxArray[i].setLabel(index + 1);
                        continue;
                    }

                    currentState = taskBoxArray[i].getState();
                    currentLabel = taskBoxArray[i].getLabel();
                    
                    taskBoxArray[i].setState(prevState);
                    taskBoxArray[i].setLabel(parseInt(prevLabel));

                    prevState = currentState;
                    prevLabel = currentLabel;
                }
            }
        }

        taskPanel.add(taskCounter);

        taskPanel.loadCounter = 0;
        taskPanel.current = 0;

        var boxCount = taskDataCollection.length > 5 ? 5 : taskDataCollection.length;

        taskPanel.setLoaded = function(){
            taskPanel.loadCounter++;
            if(taskPanel.loadCounter == boxCount){
                taskBoxLayer.batchDraw();
            }
        }

        var currentBox;

        for(var i = 0; i < boxCount; i++){
            currentBox = createTaskBox(0 + 30 * i, 0, i);
            taskPanel.add(currentBox);
        }

        taskBoxLayer.add(taskPanel);
    }

    function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

});