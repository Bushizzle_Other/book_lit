var colorSheme = {
	"scrollBar":{
		"trackColor": "#97C25A",
		"arrowColor": "#97C25A",
		"innerSliderColor": "#97C25A",
		"outerSliderColor": "#97C25A"
	},
	"scrollArrow":{
		"color": "#D7D7D7",
		"stroke": "",
		"strokeWidth": 0
	},
	"toolTipRectangle":{
		"backgroundColor": "#82C351",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 0
	},
	"bottomPanel":{
		"color": "#DCE6D1",
		"stroke":"#C0DEA8"
	},
	"crossWordSymbol":{
		"clear":{
			"backgroundColor": "white",
			"cornerRadius": 0,
			"stroke": "#adadad",
			"strokeWidth": 1,
			"fontColor": "black"
		},
		"active":{
			"backgroundColor": "#9DCF77",
			"stroke": "#ccd6bd",
			"fontColor": "white",
			"strokeWidth": 2
		},
		"focused":{
			"backgroundColor": "#428A00",
			"stroke": "#ccd6bd",
			"fontColor": "white",
			"strokeWidth": 2
		},
		"wrong":{
			"backgroundColor": "#FF7B7B",
			"stroke": "#FF0033"
		},
		"constant":{
			"stroke": "#ffff5a",
			"strokeWidth": 2
		}
	},
	"crosswordPanel":{
		"backgroundColor": "white",
		"stroke": "",
		"cornerRadius": 0
	},
	"tipPanel":{
		"backgroundColor": "white",
		"stroke": "",
		"cornerRadius": 0,
		"lineColor": "#97c25a",
		"glowedRect":{
			"color": "#EDF2E8",
			"stroke": "#C0DEA8",
			"strokeWidth": 0.8
		},
		"tipIcon":{
			"backgroundColor": "#82C351",
			"fontColor": "white"
		}
		
	},
	"keyboard":{
		"backgroundColor": "white",
		"stroke": "#97c25a",
		"strokeWidth": 0.8,
		"cornerRadius": 0,
		"keyboardIcon": "keyIconGreen.png",
		"closeIcon": "closeIcon.png"
	},
	"zoomIcon": "zoomIcon.png",
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#9DCF77",
		"strokeWidth": 0.9,
		"cornerRadius": 6
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"player":{
		"backgroundRectColor": "black",
		"trackBackroundColor": "#4B4B4B",
		"trackColor": ["white", "white"],
		"opacity": 0.7
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a"
		}
	}
}