var colorSheme = {
	"scrollBar":{
		"trackColor": "#97c25a",
		"arrowColor": "#97c25a",
		"innerSliderColor": "#97c25a",
		"outerSliderColor": "#97c25a",
		"backgroundColor": "white"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#97c25a",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"answerContainer":{
		"color": "white",
		"stroke": "#97c25a",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"tableContainer":{
		"color": "white",
		"stroke": "#97c25a",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"answerBlock":{
		"color": "#82C351",
		"stroke": "",
		"strokeWidth": 1,
		"fontColor": "white",
		"cornerRadius": 4
	},
	"staticTableBlock":{
		"color": "#82C351",
		"stroke": "#82C351",
		"strokeWidth": 1,
		"fontColor": "white",
		"cornerRadius": 0
	},
	"tableBlock":{
		"color": "white",
		"stroke": "#d6d6d6",
		"strokeWidth": 1,
		"fontColor": "black",
		"cornerRadius": 0
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a"
		}
	}
}