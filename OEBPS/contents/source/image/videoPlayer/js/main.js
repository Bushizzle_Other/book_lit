var isResizing = false,
    ratio = 0;

$(document).ready(function () {

    var vid = document.createElement('video');
    vid.src = location.href.split('?config=')[1];
    vid.controls = true;
    vid.id = 'video';

    ////console.log(vid);

    function resizeVideo(){
        isResizing = true;

        if(window.innerWidth > window.innerHeight*ratio){
            $(vid).css({height: '90%', width: 'auto', margin: '0 auto'});
        } else {
            $(vid).css({width: '90%', height: 'auto', margin: '5vh 5% 0'});
        }

        setTimeout(function(){isResizing = false}, 500);
    }

    vid.oncanplay = function(){

        $('body').append(vid);

        ////console.log(vid.duration);

        ratio = $(vid).width()/$(vid).height();
        ////console.log(ratio);

        $(window).resize(resizeVideo);
        resizeVideo();

        vid.play();

    };
});