$(window).load(function () {

	$(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

	Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    function reorient(e) {
      window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

	var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';
	events[0].mousedown = 'mousedown';
	events[1].mousedown = 'tap';

	/*Глобальные переменные**/
	
	var COLUMN_COUNT;

	/*Глобальный флаг, характеризующий движение мыши с зажатой кнопкой по канве**/
	var moving = false;	
	/*Отпустили ли левую кнопку мыши над узлом**/
	var isMouseUpOnNode = false;
	/*Узел из которого в данный момент рисуется связь**/
	var currentNode = null;
	/*Текущие связи, заполняются пользователем**/
	var connections = [];
	/*Связи-ответы, берутся из xml**/
	var answers = [];
	/*Отрисовываемая в данный момент линия**/
	var currentLine = null;

	var aspectRatio = 0; //widescreen 16:9

	var defaultStageWidth = 1024;
	var defaultStageHeight = 576;

	var contentMargin = {x: 5, y: 5};

	var contentFontSize = 15;

	var marginBot = 0;

	var assetsCollection = [];

	var root = '';

	var mainTitle = null;

	var columnTitleGroup = new Kinetic.Group({
		x: 0,
		y: defaultStageHeight
	});

	var colorSheme = null;

	var levelManager = new LevelManager();

    var mainFont = 'mainFont';

	skinManager = new SkinManager('assets/skin', function(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    });

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(),  $(window).height());
	}

	function initAspectRatio(){
		if($(window).width() / $(window).height() < 1.6){
			aspectRatio = 1;
			defaultStageWidth = 1024;
			defaultStageHeight = 768;

			stage.width(defaultStageWidth);
			stage.width(defaultStageHeight);
		}
	}

	function scaleScreen(width, height){
		var marginX = 0;
		var marginY = 0;

		width -= marginX;
		height -= marginY;

		var scalerX = width / defaultStageWidth;
		var scalerY = height / defaultStageHeight;
		var scaler = Math.min(scalerX, scalerY);
		
		stage.scaleX(scaler);
		stage.scaleY(scaler);

		stage.scaler = scaler;

		stage.width(width);
		stage.height(height);

		stage.draw();
	}

	function initWindowSize(){		
		scaleScreen($(window).width(), $(window).height());
	}

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var nodeLayer = new Kinetic.Layer();
	var achivementLayer = new Kinetic.Layer();
	var lineLayer = new Kinetic.Layer();
	var pointLayer = new Kinetic.Layer();	
	var imageContainer = new Kinetic.Layer();
	var resultLayer = new Kinetic.Layer();

    /*Заливает фон белым цветом**/
    function createBackground(){
    	var background = new Kinetic.Rect({
            x: 0, 
            y: 0, 
            width: stage.getWidth(),
            height: stage.getHeight(),
            fill: "white"
        });

        backgroundLayer.add(background);
    }
    /*Конструирует кнопку "Ответ"**/

    function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 26,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        backgroundLayer.add(buttonGroup);
        backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mousedown, resetPointer);

        return buttonGroup;
    }

    function checkAnswers(evt){
    	var isWin = true;
    	var wrongBonds = connections;

    	for(var i=0; i<answers.length;i++){
    		//Если ответ содержится в связях, исключаем данную связь из набора чтобы определить неправильные связи (если они есть)
    		var search = searchBond(answers[i]);
    		////console.log(search);
    		if(search!=-1){
    			wrongBonds.splice(search, 1);      			
    		} else{//Если ответа нету в связях - отображаем его на канве
    			//showAnswerNode(rootNode, answers[rootNode][i], lineLayer);
    			drawMissedBond(answers[i]);
    			////console.log('Miss: '+answers[i]);
    			isWin = false;
    		}
    	}
    	//Перебираем оставшиеся (неправильные) связи и помечаем их красным
    	for (i = 0; i<wrongBonds.length; i++) {
    		wrongBonds[i].line.stroke(colorSheme.bonds.wrong);
    		wrongBonds[i].line.from.fillPoint(colorSheme.bonds.wrong);
    		wrongBonds[i].line.to.fillPoint(colorSheme.bonds.wrong);
    		lineLayer.draw();
    		isWin = false;
    	}
    	
    	var button = evt.target;
    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);

    	backgroundLayer.draw();

    	resultScreen.invoke(isWin);

    	//showResultLabel($(window).width() / stage.scaler - 430, $(window).height() / stage.scaler - 40 - screenMarginY, isWin);
    }
    function drawMissedBond(bond){
    	var firstNodes = nodeLayer.find('.'+bond[0]);
    	var secondNodes = nodeLayer.find('.'+bond[1]);
		for(var i = 0; i<firstNodes.length; i++){
			for(var j=0; j<secondNodes.length; j++){
				if(isBondable(firstNodes[i], secondNodes[j])){
					createBond(firstNodes[i], secondNodes[j]);
					return;
				}
			}
		}
    }
    function createBond(firstNode, secondNode){
    	currentLine = new Kinetic.Line({
			            points: [firstNode.x(), firstNode.y(), secondNode.x(), secondNode.y()],
			            stroke: colorSheme.bonds.missed,
			            strokeWidth: 4
			        });
    	lineLayer.add(currentLine);
    	lineLayer.draw();

    	firstNode.fillPoint(colorSheme.bonds.missed);
    	secondNode.fillPoint(colorSheme.bonds.missed);

    	firstNode.showPoint();
    	secondNode.showPoint();
    }

    function isBondable(firstNode, secondNode){
    	return ((firstNode.avaibleBond == secondNode.columnIndex) && (secondNode.avaibleBond == firstNode.columnIndex))
    }

    /*Соединяет два узла зелёной линией, указывая пропущенные пользователем связи   
	 * @param {string} rootNode главный узел (LEFT_NODE или RIGHT_NODE), из которого будет рисоваться связь
	 * @param {string} node узел-ответ, в который будет входить связь
	 */
    function showAnswerNode(rootNode, node){
    	var fromNode = nodeLayer.find('#'+rootNode+'_'+rootNode)[0];
    	var toNode = nodeLayer.find('#'+rootNode+'_'+node)[0];
    	currentLine = new Kinetic.Line({
			            points: [fromNode.x(), fromNode.y(), toNode.x(), toNode.y()],
			            stroke: "green",
			            strokeWidth: 4
			        });
    	lineLayer.add(currentLine);
    	lineLayer.draw();
    }	

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    /*Вызывается по окончанию загрузки данных. Инициирует данными глобальные переменные а также элементы интерфейса**/
    function xmlLoadHandler(xml) {
    	initAspectRatio();
    	initWindowSize();

    	initStage();

    	resultScreen = initResultScreen();

		root = $(xml).children('component[type=connection]');

		levelManager.initRoute(root);

		loadAssets(root.children('columns').children('column'), startApp);
	}

	function startApp(){
    	createBackground();

		var title = root.children('title').text();
		var question = root.children('question').text();
		var columns = root.children('columns').children('column');

		var fontSize = $(root).attr('fontSize');

		contentFontSize = typeof fontSize == 'undefined' ? 15 : fontSize;

		var bonds = root.children('bonds').children('bond');
		
		COLUMN_COUNT = columns.length;
		
		if(COLUMN_COUNT < 2) throw new Error('Wrong column count!');

		answers = getAnswers(bonds);
		mainTitle = createHead(title, question);

		backgroundLayer.add(mainTitle);

		createButton((($(window).width() / stage.scaler) >> 1) - 90, ($(window).height() / stage.scaler) - 45 - screenMarginY, 180, 40, 'Ответ', checkAnswers);
		marginBot = 60;

		buildColumns(columns);

		//createBackground();		

		initImageContainer();
	}

	function loadAssets(columns, onAssetsLoaded){
		var pictures;
		var imageObj;
		var imageSource = '';
		var imageLoadCounter = 0;

		$(columns).each(function(index){
			//column

			assetsCollection[index] = [];

			pictures = $(this).children('statement[picture]');

			$(pictures).each(function(pictureIndex){
				imageSource = levelManager.getRoute($(this).attr('picture'));

				if(imageSource == '') return;

				imageLoadCounter++;

				imageObj = new Image();

				imageObj.onload = function(){
					imageLoadCounter--;

					if(imageLoadCounter == 0) onAssetsLoaded();
				}

				assetsCollection[index][$(this).attr('id')] = imageObj;

				imageObj.src = imageSource;
			});
		});

		if(imageLoadCounter == 0) onAssetsLoaded();
	}

	/*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
	 */
	function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        //imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: questionLabel.height() + questionLabel.fontSize() * 0.1 + (padding << 1),
                stroke: headSheme.questionTitle.stroke,
                strokeWidth: headSheme.questionTitle.strokeWidth
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

	/*Функция конструирует столбцы
	* @param {element} columns xml набор столбцов
	**/
	function buildColumns(columns){
		var marginTop = mainTitle.height() + 15;
		var marginLeft = 5;

		//размеры контейнера, в котором будут размещаться столбцы
		var containerHeigth = $(window).height() / stage.scaler - marginBot - marginTop;
		var containerWidth = $(window).width() / stage.scaler - (marginLeft << 2);

		var columnSpacer; //расстояние между столбцами

		switch(COLUMN_COUNT){
			case 2: columnSpacer = 400;
				break;
			case 3: columnSpacer = 135;
				break;
			default: columnSpacer = 60;
				break;
		}

		columnSpacer = 320 / COLUMN_COUNT;

		if(columnSpacer < 80) columnSpacer = 80;
		
		var columnWidth = (containerWidth - columnSpacer * (COLUMN_COUNT - 1)) / COLUMN_COUNT;

		var dx = marginLeft + 5;
		
		columns.each(function(index){
			createColumn(dx, marginTop, $(this), index, columnWidth, containerHeigth);
			dx += columnWidth + columnSpacer;
		});

		backgroundLayer.add(columnTitleGroup);
		backgroundLayer.draw();
	}
	/*Конструирует столбец
	* param {number} x положение столбца по x
	* param {number} y положение столбца по y
	* param {element} xml набор, содержащий элементы конкретного столбца
	* param {number} columnIndex индекс столбца
	* param {number} columnWidth ширина столбца
	* param {number} containerHeigth высота контейнера (максимальная высота столбца)
	**/
	function createColumn(x, y, column, columnIndex, columnWidth, containerHeigth){
		var columnElementsCount = column.children('statement').length;
		var pictureCount = column.children('statement[picture]').length;
		var labelCount = columnElementsCount - pictureCount;
		
		var verticalSpacer = 10; //Вертикальный отступ между элементами
		
		var statements = shuffle(column.children('statement'));

		var totalLabelHeigth = 0; //Высота всех текстовых меток
		var labelTextCollection = []; //Коллекция текстовых объектов

		var pictureSource;

		var align = calculateAlign(columnIndex);

		var title = createColumnTitle(x, 0, column.children('title').text(), columnWidth, 'center');

		var maxHeight = (containerHeigth - title.titleHeight) / columnElementsCount - verticalSpacer;

		//Вычисляем высоты всех текстовых меток, попутно занося их объекты в коллекцию
		for(var i = 0; i < statements.length; i++){

			pictureSource = $(statements[i]).attr('picture');

			if(!(typeof pictureSource !== 'undefined' && pictureSource !== false)){				

				var labelObject = new Kinetic.Text({
					width: columnWidth,
					text: $(statements[i]).text(),
					fontSize: contentFontSize,
					fontFamily: mainFont,
					fill: 'black',
					align: 'center'
				  });

				if(labelObject.height() > maxHeight) labelObject.height(maxHeight);

				labelObject.value = $(statements[i]).attr('id');
				labelTextCollection.push(labelObject);
				totalLabelHeigth += labelObject.height() + verticalSpacer;
			}
		}

		var columnHeigth = totalLabelHeigth - verticalSpacer; //Пространство, занимаемое метками и пробелами
		//Если в столбце есть изображения - вычисляем для них максимально допустимую высоту
		if(pictureCount != 0){
			/*var pictureSize = maxHeight; //максимальный размер изображения
			var columnPictureSize = pictureCount * pictureSize; //пространство, занимаемое картинками по высоте

			columnHeigth += columnPictureSize;

			var verticalScaler = columnPictureSize>0?Math.abs((containerHeigth - y - totalLabelHeigth - verticalSpacer * (columnElementsCount - 1)) / columnPictureSize):1;
			if(verticalScaler < 1){
				pictureSize *= verticalScaler;
				columnHeigth = pictureCount * pictureSize + totalLabelHeigth+verticalSpacer*(columnElementsCount-1);
			}*/

			var pictureSize = ((containerHeigth - columnHeigth - title.titleHeight - verticalSpacer * pictureCount) / pictureCount);

			if(pictureSize < maxHeight) pictureSize = maxHeight;

			////console.log('PS ' + pictureSize + ' __ ' + containerHeigth + ' __ ' + pictureCount + ' __ ' + maxHeight + ' __ ' + (containerHeigth - columnHeigth - (title.height() * title.fontSize() * 0.1 + 10)) + ' ___ ' + (title.height() * title.fontSize() * 0.1 + 10));

			////console.log('S! ' + ((containerHeigth - (title.titleHeight - columnHeigth)) / pictureCount) + ' ___ ' + columnHeigth + ' ___ ' + pictureSize);

			//columnHeigth += pictureSize * pictureCount;
		}

		var avaibleSpaceH = containerHeigth - title.titleHeight;

		var contentY = y + title.titleHeight;

        var columnSheme = colorSheme.column;

		var tempRect = new Kinetic.Rect({
			x: x,
			y: y,
			width: columnWidth,
			height: containerHeigth,
			fill: columnSheme.backgroundColor,
			stroke: columnSheme.stroke,
			strokeWidth: columnSheme.strokeWidth,
			//cornerRadius: 8
		});

		backgroundLayer.add(tempRect);
		backgroundLayer.draw();

		var currentLabel;
		var currentImage;

		var pictures = column.children('statement[picture]');

		var totalPictureSize = 0;
		var pictureCollection = [];

		pictures.each(function(index){
			currentImage = createPicture(x, 0, $(this).attr('id'), columnIndex, pictureSize, columnWidth);
			totalPictureSize += currentImage.height() * currentImage.scaleX() + verticalSpacer;

			pictureCollection[$(this).attr('id')] = currentImage;
		});

		if(pictures.length > 0) columnHeigth += totalPictureSize;

		//Вписываем столбец в центр контейнера по высоте (если вмещается)
		var contentPostionY = (avaibleSpaceH > columnHeigth) ? (contentY + (avaibleSpaceH >> 1) - (columnHeigth >> 1)) : contentY;
		var dy = contentPostionY;
		var dx = 0;

        var someRect = new Kinetic.Rect({
            x: x,
            y: contentY,
            width: columnWidth,
            height: avaibleSpaceH,
            fill: 'black'
        })

		currentImage = null;

		//Создаём изображения и расставляем по местам метки
		for(i = 0; i < statements.length; i++){
			
			pictureSource = $(statements[i]).attr('picture');
			if(typeof pictureSource !== 'undefined' && pictureSource !== false){			
				currentImage = pictureCollection[$(statements[i]).attr('id')];
				currentImage._setPosition(x, dy);

				dy += (currentImage.height() * currentImage.scaleX()) + verticalSpacer;
			} else{
				currentLabel = labelTextCollection.pop();

				currentLabel.x(x);
				currentLabel.y(dy);

				initLabel(currentLabel, columnIndex, columnWidth);
				dy += currentLabel.height() + verticalSpacer;
			}
		}

		//if(columnTitleGroup.y() > contentPostionY - 60) columnTitleGroup.y(contentPostionY - 60);
		columnTitleGroup.y(y);

		//createColumnTitle(x, 0, column.children('title').text(), columnWidth, align);

        //achivementLayer.add(someRect);

		achivementLayer.draw();
	}
	function calculateAlign(columnIndex){
		if(columnIndex == 0){
			return 'right';
		} else if(columnIndex==(COLUMN_COUNT-1)){
			return 'left';
		}

		return 'center';
		
	}
	function createColumnTitle(x, y, title, columnWidth, align){
        var titleSheme = colorSheme.column.title;

        var padding = 10;

		var columnTitle = new Kinetic.Text({
			x: x,
			y: y + padding,
			text: title,
			width: columnWidth,
			fontSize: contentFontSize * 4/3,
			fontFamily: mainFont,
			fontStyle: 'bold',
			fill: titleSheme.fontColor,
			align: align
		});

		var titleHeight = columnTitle.height() + columnTitle.fontSize() * 0.2;

        var columnTitleRect = new Kinetic.Rect({
            x: x,
            y: y,
            width: columnWidth,
            height: columnTitle.height() + (padding << 1),
            fill: titleSheme.color
        })

		/*var columnTitleBorder = new Kinetic.Line({
			points: [x, y + titleHeight, x + columnTitle.width(), y + titleHeight],
			stroke: 'black',
			strokeWidth: 0.2
		})*/
		
		columnTitleGroup.add(columnTitleRect);
		columnTitleGroup.add(columnTitle);
		//backgroundLayer.draw();

        columnTitle.titleHeight = columnTitleRect.height();

		return columnTitle;
	}

	function initLabel(labelObject, columnIndex, columnWidth){
		achivementLayer.add(labelObject);

		var leftPosition = [labelObject.x()-20, labelObject.y()+(labelObject.height()*labelObject.scaleY())/2];
		var rightPosition = [labelObject.x() + columnWidth+20, labelObject.y()+(labelObject.height()*labelObject.scaleY())/2];
		
		if(columnIndex!=0&&columnIndex!=COLUMN_COUNT-1){ //Узлы с двух сторон
			buildNode(leftPosition[0], leftPosition[1], labelObject.value, - 1, columnIndex);
			buildNode(rightPosition[0], rightPosition[1], labelObject.value, 1, columnIndex);
		} else{ //Узел с одной из сторон
			var position = (columnIndex == 0) ? rightPosition : leftPosition;
			buildNode(position[0], position[1], labelObject.value, (columnIndex == 0) ? 1 : - 1, columnIndex);
		}
	}

	function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

	    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

	    return ratio;
	 }

	function createPicture(x, y, id, columnIndex, maxSize, columnWidth){
		var imgObj = assetsCollection[columnIndex][id];

		////console.log(assetsCollection);

		imgObj.id = 'img_' + id;

		var image = new Kinetic.Image({
		  x: x,
		  y: y,
		  image: imgObj,
		  width: imgObj.width,
		  height: imgObj.height,
		  name: 'img_'+columnIndex,
		  lineCap: 'round'
		});			

		image.on(events[isMobile].mouseover, hoverPointer);
		image.on(events[isMobile].mouseout, resetPointer);

		_maxSize = Math.min(maxSize, columnWidth);

		var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, _maxSize, _maxSize);

		image.scaleX(scaler);
		image.scaleY(scaler);
		image.scaler = scaler;

		var imageWidth = image.width() * image.scaleX();
		var imageHeight = image.height() * image.scaleY();

		if(imageWidth < columnWidth) image.x(x+((columnWidth >> 1) - (imageWidth >> 1)));
		//if(imageHeight < maxSize) image.y(y+((maxSize >> 1) - (imageHeight >> 1)));

		image.defaultPositionX = image.x();
		image.defaultPositionY = image.y();

		image.on(events[isMobile].click, imageZoomHandler);
		image.isZoomed = false;

		backgroundLayer.add(image);

		image._setPosition = function(x, y){
			//image.x(x);
			image.y(y);

			var leftPosition = [x-20, image.y()+((image.height()*image.scaleY())>>1)];
			var rightPosition = [x + columnWidth + 20, image.y()+((image.height()*image.scaleY())>>1)];
			if(columnIndex!=0&&columnIndex!=COLUMN_COUNT-1){ //Узлы с двух сторон
				buildNode(leftPosition[0], leftPosition[1], id, -1, columnIndex);
				buildNode(rightPosition[0], rightPosition[1], id, 1, columnIndex);
			} else{ //Узел с одной из сторон
				var position = (columnIndex==0) ? rightPosition : leftPosition;
				buildNode(position[0], position[1], id, (columnIndex==0)?1:-1, columnIndex);
			}

			backgroundLayer.draw();
		}

		/*Скалирует изображения по клику**/
		function imageZoomHandler(evt){
			var currentImg = evt.target;
			imageContainer.zoomImage(currentImg.getImage());				      
		}

		////console.log('SUPAR HEIGHT ' + imageHeight + ' --- ' + _maxSize);

		return image;
	}

	function searchBond(bond){
		for(var i = 0; i<connections.length; i++){
			if((connections[i].bond[0] == bond[0] && connections[i].bond[1] == bond[1]) ||
				(connections[i].bond[1] == bond[0] && connections[i].bond[0] == bond[1])) return i;
		}
		return - 1;
	}
	
	function buildNode(x, y, id, columnSide, columnIndex){
    	var point = new Kinetic.Circle({
	        x: x,
	        y: y,
	        radius: 6,
	        fill: colorSheme.bonds.regular,
	        strokeWidth: 1
	      });

    	point.hide();

    	var node = new Kinetic.Circle({
	        x: x,
	        y: y,
	        radius: 10,
	        stroke: colorSheme.nodeColor,
	        strokeWidth: 1,
	        name: id
	      });
		
		node.value = id;
		node.columnIndex = columnIndex;
		node.avaibleBond = columnSide + columnIndex;
		node.isBusy = false;
		nodeLayer.add(node);

		node.on(events[isMobile].down, onNodeClick);
		node.on(events[isMobile].up, onNodeUp);

		node.on(events[isMobile].mouseover, hoverPointer);
		node.on(events[isMobile].mouseout, resetPointer);

		nodeLayer.draw();

		pointLayer.add(point);
		pointLayer.draw();


		node.showPoint = function(){
			point.show();
			pointLayer.draw();
		}
		node.hidePoint = function(){
			point.hide();
			pointLayer.draw();
		}
		node.fillPoint = function(color){
			point.fill(color);
			pointLayer.draw();
		}

		//Обработчик срабатывает когда пользователь отпускает связь над другим узлом
		function onNodeUp(evt){
			if(!moving) return;

            var targetNode = evt.target;

            //Ставим связь в центр узла
            currentLine.points()[2] = targetNode.x();
            currentLine.points()[3] = targetNode.y();

			currentLine.points().splice(4,5);

            lineLayer.draw();

			if(!isBondable(currentNode, targetNode)) return;

			var firstIndex;
			var secondIndex;

			if(currentNode.value>targetNode.value){
				firstIndex = targetNode.value;
				secondIndex = currentNode.value;
			} else{
				firstIndex = currentNode.value;
				secondIndex = targetNode.value;
			}
			if(searchBond([firstIndex, secondIndex])!=-1) return;

			isMouseUpOnNode = true;
			connections.push({
				"bond": [firstIndex, secondIndex],
				"line": currentLine
			});

			targetNode.showPoint();
			currentNode.isBusy = targetNode.isBusy = true;

			currentLine.from = currentNode;
			currentLine.to = targetNode;
		}

		/*Срабатывает по клику на произвольном узле**/
		function onNodeClick(evt){
            if (moving){
                moving = false;
                //lineLayer.draw();
            } else {
            	var targetNode = evt.target;

            	currentLine = new Kinetic.Line({
	                points: [0, 0, 0, 0],
	                stroke: colorSheme.bonds.regular,
	                strokeWidth: 4
	            });
                var mousePos = stage.getPointerPosition();
                
                currentLine.points()[0] = currentLine.points()[2] = targetNode.x();
                currentLine.points()[1] = currentLine.points()[3] = targetNode.y();

                moving = true;    
                //lineLayer.drawScene();
                isMouseUpOnNode = false;
                currentNode = targetNode;
                lineLayer.add(currentLine);
                if(!targetNode.isBusy) targetNode.showPoint();
            }

            lineLayer.drawScene();
        }
    }

    function getAnswers(bonds){
    	var answers = [];

    	$(bonds).each(function(index){
    		answers[index] = [];
    		if(+$(this).attr('id1')>+$(this).attr('id2')){
    			answers[index][0] = +$(this).attr('id2');
    			answers[index][1] = +$(this).attr('id1');
    		} else{
    			answers[index][0] = +$(this).attr('id1');
    			answers[index][1] = +$(this).attr('id2');
    		}
    	});
    	return answers;
    }

	function getRelativePointerPosition() {
	    var pointer = stage.getPointerPosition();
	    var pos = stage.getPosition();
	    var offset = stage.getOffset();
	    var scale = stage.getScale();

	    return {
	        x : ((pointer.x / scale.x) - (pos.x / scale.x) + offset.x),
	        y : ((pointer.y / scale.y) - (pos.y / scale.y) + offset.y)
	    };
	}

	/*Инициализирует сцену:
	  добавляет слои в список отображения
	  ставит прослушиватели событий**/
	function initStage(){
		stage.add(backgroundLayer);
	    stage.add(achivementLayer);
	    stage.add(lineLayer);	    
	    stage.add(pointLayer);
	    stage.add(nodeLayer);
	    stage.add(imageContainer);
	    stage.add(resultLayer);

	    lineLayer.drawScene.process();

	    /*При движении мыши с зажатой левой кнопкой от произвольного узла тянет за ней вектор-связь**/
        stage.on(events[isMobile].move, function(){
            if (moving) {
                var mousePos = getRelativePointerPosition();

                var angle = Math.atan2(mousePos.y - currentLine.points()[1], mousePos.x - currentLine.points()[0]);
                var headlen = 12;
                //var headlen = 8;

				currentLine.points()[2] = mousePos.x;
                currentLine.points()[3] = mousePos.y;
                currentLine.points()[4] = mousePos.x - headlen * Math.cos(angle - Math.PI/6);
                currentLine.points()[5] = mousePos.y - headlen * Math.sin(angle - Math.PI/6);
                currentLine.points()[6] = mousePos.x + 5 * Math.cos(angle);
                currentLine.points()[7] = mousePos.y + 5 * Math.sin(angle);
                currentLine.points()[8] = mousePos.x - headlen * Math.cos(angle + Math.PI/6);
                currentLine.points()[9] = mousePos.y - headlen * Math.sin(angle + Math.PI/6);

                lineLayer.drawScene();
            }
        });
        /*Если пользователь "отпустил"" связь не над свободным узлом - убираем её**/
        stage.on(events[isMobile].up, function(){
            if(moving){
            	moving = false;
	            if(!isMouseUpOnNode){
	            	currentLine.points()[2] = currentLine.points()[0];
	            	currentLine.points()[3] = currentLine.points()[1];

	            	currentLine.points([0, 0, 0, 0]);
	            	if(!currentNode.isBusy) currentNode.hidePoint();

	            	lineLayer.batchDraw();
	            }
	        }
        });
	}
    /*Отображает сообщение-результат игры
    * @param {number} x определяет положение сообщения в пространстве по оси x
    * @param {number} y определяет положение сообщения в пространстве по оси y
    * @param {boolean} isWin флаг определяет результат игры
    * false - проиграл
    * true - выиграл
    */
    function showResultLabel(x,y,isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    /*Перемешивает элементы массива o
    * @param {array} o исходный массив
    * @return {array} o массив с перемешанными элементами
    */
	function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function initImageContainer(){
    	var image = new Kinetic.Image({
		  x: 20,
		  y: 20
		});

		var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		var modalGroup = new Kinetic.Group();

		modalRect.opacity(0.8);

		image.on(events[isMobile].mouseover, hoverPointer);
    	image.on(events[isMobile].mouseout, resetPointer);

		image.hide();
		modalRect.hide();

		modalGroup.add(modalRect);
		modalGroup.add(image);

		imageContainer.add(modalGroup);

		imageContainer.zoomImage = function(imageObj){

			image.setImage(imageObj);

			var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight-40);

			image.width(imageObj.width * scaler);
			image.height(imageObj.height * scaler);

			if(!image.isVisible()){
				image.x(defaultStageWidth >> 1);
		    	image.y(defaultStageHeight >> 1);

		    	image.scale({
		    		x: 0,
		    		y: 0
		    	})

		    	var inTween = new Kinetic.Tween({
		    		node: image,
		    		scaleX: 1,
		    		scaleY: 1,
		    		x: (($(window).width() / stage.scaler) >> 1) - (image.width() >> 1),
		    		y: (($(window).height() / stage.scaler) >> 1) - (image.height() >> 1),
					duration: 0.4,
					easing: Kinetic.Easings.EaseInOut
		    	})
		    	inTween.onFinish = function(){
		    		//modalRect.show();
		    	}

		    	modalRect.show();

				image.show();
		    	inTween.play();
			}

			modalGroup.on(events[isMobile].click, zoomOut);

			imageContainer.draw();
		}

		function zoomOut(evt){
			
	    	var outTween = new Kinetic.Tween({
	    		node: image,
	    		scaleX: 0,
	    		scaleY: 0,
	    		x: defaultStageWidth>>1,
	    		y: defaultStageHeight>>1,
				duration: 0.4,
				easing: Kinetic.Easings.EaseInOut
	    	});

	    	outTween.onFinish = function(){
		    	image.hide();
				imageContainer.draw();
			}

			modalRect.hide();

	    	outTween.play();
		}
    }

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                }

                outTween.play();
            });


            resultScreen.add(resultButton);

            resultLayer.draw();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        }

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        }

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        }

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

    function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }
});