function SkinManager(skinFolder, initCallback) {
    var instance = this;
    initGraphicQuality($(window).width(), $(window).height());
    importJS(skinFolder + '/colorSheme.js', 'colorSheme', function() {
        initColorSheme(colorSheme);
    });

    function onFail() {
        ////console.log(arguments);
    }

    function initColorSheme(data) {
        instance.colorSheme = data;
        initCallback();
    };
    this.getGraphic = function(relativeGraphicPath) {
        return skinFolder + '/' + instance.graphicQuality + '/' + relativeGraphicPath;
    };
    this.getScaleFactor = function() {
        return instance.scaleFactor;
    };

    function initGraphicQuality(screenWidth, screenHeight) {
        var ld = 480 * 288;
        var sd = 1024 * 768;
        var hd = 1440 * 1080;
        var screenSize = screenWidth * screenHeight;
        if (screenSize <= ld) {
            instance.graphicQuality = 'ld';
        } else if (screenSize > ld && screenSize <= sd) {
            instance.graphicQuality = 'sd';
        } else {
            instance.graphicQuality = 'hd';
        }
        instance.graphicQuality = 'hd';
        switch (instance.graphicQuality) {
            case "hd":
                instance.scaleFactor = 2;
                break;
            case "sd":
                instance.scaleFactor = 1;
                break;
            case "ld":
                instance.scaleFactor = 0.5;
                break;
            default:
                instance.scaleFactor = 1;
                break;
        }
        ////console.log('Using ' + instance.graphicQuality + ' textures!');
    }
}