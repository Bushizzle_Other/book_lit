var ua = navigator.userAgent;
var isIOS = (/iPhone|iPad|iPod/i.test(ua));
var isAndroid = ua.indexOf("android") > -1;
var isTouchDevice = (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
var isWindowsTablet = (!isAndroid && !isIOS);
var coreLib = {};

if(typeof Kinetic == 'undefined' && typeof Konva == 'undefined'){
    coreLib.pixelRatio = 1;
} else {
    coreLib = (typeof Kinetic == 'undefined') ? Konva : Kinetic;
}


var HookManager = function() {
    this.hooks = {
        "clearCanvasFix": clearCanvasFix,
        "windowsTabletTouchFix": windowsTabletTouchFix,
        "KitkatTouchFix": KitkatTouchFix
    };
    this.applyFix = function(fix) {
        this.hooks[fix].apply();
    };
};
var hookManager = new HookManager();
hookManager.applyFix('windowsTabletTouchFix');

function detectVerticalSquash(img) {
    var iw = img.width,
        ih = img.height;
    var canvas = document.createElement('canvas');
    canvas.width = 1;
    canvas.height = ih;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    var data = ctx.getImageData(0, 0, 1, ih).data;
    var sy = 0;
    var ey = ih;
    var py = ih;
    while (py > sy) {
        var alpha = data[(py - 1) * 4 + 3];
        if (alpha === 0) {
            ey = py;
        } else {
            sy = py;
        }
        py = (ey + sy) >> 1;
    }
    var ratio = (py / ih);
    return (ratio === 0) ? 1 : ratio;
}

function getImage(image) {
    if (!isIOS) return image;
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    canvas.width = image.width;
    canvas.height = image.height;
    var vertSquashRatio = detectVerticalSquash(image);
    context.drawImage(image, 0, 0, image.width, image.height / vertSquashRatio);
    return (canvas);
}

function windowsTabletTouchFix() {
    if (isWindowsTablet) {
        coreLib.pixelRatio = 1;
    }
}

function KitkatTouchFix() {
    if (ua.indexOf("Android") >= 0) {
        var androidversion = parseFloat(ua.slice(ua.indexOf("Android") + 8));
        if (androidversion == 4.4) {
            coreLib.pixelRatio = 1;
        }
    }
}

function clearCanvasFix() {
    if (ua.indexOf("Android") >= 0) {
        var androidversion = parseFloat(ua.slice(ua.indexOf("Android") + 8));
        if (androidversion >= 4.1 && androidversion < 4.3) {
            var clearCanvas = _.throttle(function(canvas) {
                canvas.style.opacity = 0.99;
                setTimeout(function() {
                    canvas.style.opacity = 1;
                });
            }, 100);
            var oldDraw = coreLib.Layer.prototype.drawScene;
            coreLib.Layer.prototype.drawScene = function() {
                clearCanvas(this.canvas._canvas);
                oldDraw.apply(this, arguments);
                return this;
            };
            Function.prototype.process = function(state) {
                return;
            };
        } else {
            Function.prototype.process = function(state) {
                var process = function() {
                    var args = arguments;
                    var self = arguments.callee;
                    setTimeout(function() {
                        self.handler.apply(self, args);
                    }, 0)
                };
                for (var i in state) process[i] = state[i];
                process.handler = this;
                return process;
            };
        }
    }
}