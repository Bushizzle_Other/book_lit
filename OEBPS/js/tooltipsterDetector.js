$(function() {
	if(self!=top){

		var docClock = setInterval(function(){

			if($('body').hasClass('loaded')) {
				clearInterval(docClock);

				$('.my-tooltip').tooltipster({
			contentAsHTML: true,
			theme: 'tooltipster-noir',
			trigger: 'click',
			maxWidth: '700',
			animation: 'grow'
				});
			}
		}, 300);

	} else {

		$('.my-tooltip').tooltipster({
			contentAsHTML: true,
			theme: 'tooltipster-noir',
			trigger: 'click',
			maxWidth: '500',
			offsetX: '0',
			animation: 'grow'
		});
	}
});